/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLHttpRequst__
#define COLIB_CLHttpRequst__

#include "CoLibCore.h"

NS_CL_BEGIN

USING_NS_CC;
USING_NS_STD;

typedef enum kRequestType
{
    kRequestTypeFetcher     = 1,
    kRequestTypeDownloader  = 2
}kRequestType;

typedef enum kHttpRequestType
{
    kHttpRequestTypeGet             = 1,
    kHttpRequestTypePost            = 2,
}kHttpRequestType;

#define TIMEOUT_REQUEST_DEFAULT 30
#define TIMEOUT_CONNECTION_DEFAULT 30
#define KEY_FILE_PARAMS         "file-param"

class HttpRequest : public Object
{
private:
    kRequestType            _requestType;
    kHttpRequestType        _httpReqType;
    int                     _tag;
    string                  _uRL;
    cocos2d::Dictionary     *_params;
    string                  _postBody;
    cocos2d::Dictionary     *_headers;
    
    int                     _timeOutRequest;
    int                     _timeOutConnection;

    // request downlaoder variables
    string                  _filePath;
    string                  _fileName;
protected:
    HttpRequest();
    HttpRequest(kRequestType aRequestType, string aURL,int aTag, Dictionary *theParams,kHttpRequestType reqType);
    HttpRequest(string aURL,int aTag, Dictionary *theParams, string aFilePath, string aFileName,kHttpRequestType reqType); // request downloader
    HttpRequest(string aURL,int aTag, Dictionary *theParams,kHttpRequestType reqType); // request fetcher
    
public:
    static HttpRequest* create(string aURL,int aTag, Dictionary *theParams, string aFilePath, string aFileName,kHttpRequestType reqType = kHttpRequestTypeGet);
    static HttpRequest* create(string aURL,int aTag, Dictionary *theParams,Dictionary *fileParam,string aFilePath, string aFileName,kHttpRequestType reqType = kHttpRequestTypeGet);
    static HttpRequest* create(string aURL,int aTag, Dictionary *theParams,kHttpRequestType reqType = kHttpRequestTypeGet);
    static HttpRequest* create(string aURL,int aTag, Dictionary *theParams,Dictionary *fileParams,kHttpRequestType reqType = kHttpRequestTypeGet);
    
    kRequestType     getRequestType();
    int              getTag();
    string           getURL();
    Dictionary*      getParams();
    int              getTimeoutRequest();
    int              getTimeoutConnection();
    kHttpRequestType getHttpRequestType();
    string           getPostBody();
    cocos2d::Dictionary*  getHeaders();
    
    void             setURL(string aURL);
    void             setCCDictionary(Dictionary *aParams);
    void             setTimeoutRequest(int aTimeoutRequest);
    void             setTimeoutConnection(int aTimeoutConnection);
    void             setCustomHeaders(Dictionary*);
    void             setHttpRequestType(kHttpRequestType type);
    void             setPostBody(string aPostBody);
    string           getFilePath();
    string           getFileName();
    string           getFullPath();
    string           getTempFilePath();
    void             setHeaders(cocos2d::Dictionary *headers);
    
    void setFilePath(string aFilePath);
    void setFileName(string aFileName);
    
    ~HttpRequest();
    
    static bool validate(string aUrl);
    static bool validate(string aFilePath, string aFileName);


};

NS_CL_END



#endif
