/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLConstants_h
#define COLIB_CLConstants_h

#include <iostream>

#include "CoLibConfig.h"

#include "cocos2d.h"

#define NS_CL_BEGIN     namespace CoLib {
#define NS_CL_END       }

#define USING_NS_CL     using namespace CoLib
#define USING_NS_STD    using namespace std;

#include "../Modules/Localization/CLLocalizationManager.h"
#include "../Modules/JsonManager/CLJsonManager.h"
#include "../Modules/JsonManager/CLJsonUtil.h"

NS_CL_BEGIN

// code here for other constants

NS_CL_END

#endif
