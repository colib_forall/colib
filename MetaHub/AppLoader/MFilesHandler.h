/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MFilesHandler__
#define COLIB_MFilesHandler__

#include "CoLibCore.h"
#include "CoLibHttp.h"

NS_CL_BEGIN

class MFilesHandlerProtocol;
class MAppLoader;

class MFilesHandler:public Object, CLMultiDownloaderDelegate{
friend class MAppLoader;
    
private:
    MFilesHandlerProtocol   *_delegate;
    CLMultiDownloader       *_multiDownloader;
    
    void initMultiDownloader();
    void releaseMultiDownloader();
    string getUDKeyForFileId(const char* aFileId);
    
public:
    MFilesHandler();
    ~MFilesHandler();
    
    void start(MFilesHandlerProtocol *aDelegate);
    void invalidate();
    
    void responseSucess(CLMultiDownloader *aRequest);
    void responseFailure(CLMultiDownloader *aRequest, int failCount);
    bool authSubDownload(CLMultiDownloader *aRequest, HttpResponse *aResp);
    bool authResponse(CLMultiDownloader *ref);
};

class MFilesHandlerProtocol{
public:
    virtual void fileHandlerSuccess(MFilesHandler *aFileHandler) = 0;
    virtual void fileHandlerFailed(MFilesHandler *aFileHandler) = 0;
};


NS_CL_END

#endif
