/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLMultiDownloader.h"
#include "CLHttpRequest.h"
#include "CLHttpHandler.h"

NS_CL_BEGIN

USING_NS_CC;
USING_NS_CL;

#define MAX_REQUEST_AT_A_TIME       3

pthread_mutex_t CoLib::CLMultiDownloader::_sDownloaderMutex;
int CoLib::CLMultiDownloader::_mutexInitialized = 0;

CoLib::CLMultiDownloader::CLMultiDownloader(CLMultiDownloaderDelegate *aDelegate, cocos2d::Array *theRequestArray)
{
    _multiDownloaderState = kHandlerStateInitial;
	_mdDelegate = aDelegate;
    
    cocos2d::Dictionary *duplicateRequestCheck = cocos2d::Dictionary::create();
    _downloadHandlers = new cocos2d::Dictionary();
    int colCount = theRequestArray ? theRequestArray->count() : 0;
    int requestId = 0;
    for(int i=0; i<colCount; i++)
    {
        HttpRequest* aRequestDownloader =  (HttpRequest*)theRequestArray->getObjectAtIndex(i);
        if(duplicateRequestCheck->objectForKey(aRequestDownloader->getFullPath()))
        {
            CCLOG("already contains, %s", aRequestDownloader->getFullPath().c_str());
            continue;
        }
        CCLOG("full path is %s",aRequestDownloader->getFullPath().c_str());
        duplicateRequestCheck->setObject(aRequestDownloader, aRequestDownloader->getFullPath());
        HttpHandler *nHandler = HttpHandler::create(this, aRequestDownloader);
		_downloadHandlers->setObject(nHandler, requestId);
        nHandler->setTag(requestId++);
    }
}

CoLib::CLMultiDownloader::~CLMultiDownloader()
{
    CC_SAFE_RELEASE(_downloadHandlers);
}

CLMultiDownloader* CLMultiDownloader::create(CLMultiDownloaderDelegate *aDelegate,cocos2d::Array *theRequestArray)
{
	if(!aDelegate || !theRequestArray)
    {
		return NULL;
	}
    if(_mutexInitialized == 0)
    {
        pthread_mutex_init(&_sDownloaderMutex,NULL);
        _mutexInitialized = 1;
    }
    
	CLMultiDownloader *mDownloader = new CLMultiDownloader(aDelegate, theRequestArray);
    return (CLMultiDownloader*)mDownloader->autorelease();
}

void CLMultiDownloader::start()
{
    if(_multiDownloaderState == kHandlerStateInProcess)
    {
        return;
    }
    
    _lastFailedStatusCode = kResponseStatusCodeNone;
    _multiDownloaderState = kHandlerStateInProcess;
    _successCount = 0;
	_totalItems = _downloadHandlers->count();
    _totalProcessedHandlers = 0;
    _totalRequestSent = 0;
    int toSend = MAX_REQUEST_AT_A_TIME;
	if(_totalItems < toSend)
    {
		toSend = _totalItems;
    }
	for(int i=0; i<toSend; i++)
    {
		HttpHandler *nHandler = static_cast<HttpHandler*>(_downloadHandlers->objectForKey(i));
		nHandler->setTag(i);
		if(nHandler)
        {
            _totalRequestSent++;
			nHandler->start();
		}
        else
        {
            CCLOG("null handler for %d", i);
        }
    }
}
// auto managing
void CLMultiDownloader::invalidate()
{
    DictElement* pElement = NULL;
    pthread_mutex_lock(&_sDownloaderMutex);
    CCDICT_FOREACH(_downloadHandlers, pElement)
    {
        HttpHandler *nHandler = (HttpHandler*)pElement->getObject();
        nHandler->invalidate();
    }
    pthread_mutex_unlock(&_sDownloaderMutex);
}

void CLMultiDownloader::responseSuccess(HttpResponse *ref)
{
    _successCount++;
#ifdef CL_DEBUG_ON
    CCLOG("cpp:: success for request: %d/%d, value: %s", ref->getNetworkHandler()->getTag()+1, _totalItems, ref->getNetworkHandler()->getResponseData());
#endif
    if(_mdDelegate)
    {
        _mdDelegate->responseProgress(this, _totalItems,  _successCount);
    }
    pthread_mutex_lock(&_sDownloaderMutex);
    HttpHandler *handler = static_cast<HttpHandler*> (_downloadHandlers->objectForKey(ref->getNetworkHandler()->getTag()));
	handler->invalidate();
	_downloadHandlers->removeObjectForKey(ref->getNetworkHandler()->getTag());
    pthread_mutex_unlock(&_sDownloaderMutex);
	stepForward();
}

void CLMultiDownloader::responseFailed(HttpResponse *ref,kResponseStatusCode thestatusCode)
{
#ifdef CL_DEBUG_ON
    CCLOG("cpp:: failed for request: %d/%d, reasonId: %d", ref->getNetworkHandler()->getTag()+1,_totalItems, (int)(thestatusCode));
#endif
    if(thestatusCode != kResponseStatusCodeOk)
    {
        _lastFailedStatusCode = thestatusCode;
    }
	stepForward();
}

bool CLMultiDownloader::responseAuth(HttpResponse *ref)
{
    return _mdDelegate->authSubDownload(this,ref);
}

void CLMultiDownloader::responseProgress(HttpResponse *ref, long long bytesDone, long long totalBytes,int percentage)
{
//    #ifdef CL_DEBUG_ON
//        //CCLOG("cpp:: progress for request: %d/%d, percentage: %d", ref->getNetworkHandler()->getTag()+1, totalItems, percentage);
//    #endif
}

kResponseStatusCode CLMultiDownloader::getLastFailedStatusCode()
{
    return _lastFailedStatusCode;
}
kHandlerState CLMultiDownloader::getHandlerState()
{
    return _multiDownloaderState;
}
void CLMultiDownloader::stepForward()
{
	_totalProcessedHandlers++;
	if(_totalProcessedHandlers >= _totalItems)
    {
		allProcessed();
		return;
	}
    if(_totalRequestSent >= _totalItems)
        return;
    
    pthread_mutex_lock(&_sDownloaderMutex);
	HttpHandler * nHandler = static_cast<HttpHandler*> (_downloadHandlers->objectForKey(_totalRequestSent));
	nHandler->setTag(_totalRequestSent);
	nHandler->start();
    pthread_mutex_unlock(&_sDownloaderMutex);
    _totalRequestSent++;
}

void CLMultiDownloader::allProcessed()
{
    pthread_mutex_lock(&_sDownloaderMutex);
    int count = _downloadHandlers->count();
    pthread_mutex_unlock(&_sDownloaderMutex);
    
	if(count > 0)
    {
        if(_mdDelegate)
        {
            _multiDownloaderState = kHandlerStateFailed;
            _mdDelegate->responseFailure(this, count);
        }
	}
    else
    {
        if(_mdDelegate)
        {
            if(_mdDelegate->authResponse(this))
            {
                _multiDownloaderState = kHandlerStateCompleted;
                _mdDelegate->responseSucess(this);
            }
            else
            {
                _multiDownloaderState = kHandlerStateFailed;
                _mdDelegate->responseFailure(this, kResponseStatusCodeAuthFailed);
            }
        }
	}
}
NS_CL_END