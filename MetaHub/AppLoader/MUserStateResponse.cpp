/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MUserStateResponse.h"
#include "MetaHubCore.h"
#include "MSocialUser.h"

#define M_KEY_SEQ_GLOBAL            "globalseq"

NS_CL_BEGIN

MUserStateResponse::MUserStateResponse(){
    _dictCurrencies = NULL;
    _dictScores     = NULL;
    _dictSequences  = NULL;
}

MUserStateResponse::~MUserStateResponse(){
    
}

void MUserStateResponse::clear(){
    CC_SAFE_RELEASE(_dictCurrencies);
    CC_SAFE_RELEASE(_dictScores);
}

void MUserStateResponse::processData(Dictionary* aDict){
    clear();
    
    MCR_RETAINED_ASSIGN(_dictScores, (Dictionary*)MCR_JDICT_GET_OBJECT(aDict, M_KEY_SCORES, NULL)) ;
    MCR_RETAINED_ASSIGN(_dictCurrencies, (Dictionary*)MCR_JDICT_GET_OBJECT(aDict, M_KEY_CURRENCIES, NULL)) ;
    MCR_RETAINED_ASSIGN(_dictSequences, (Dictionary*)MCR_JDICT_GET_OBJECT(aDict, M_KEY_SEQUENCES, NULL)) ;
    
    
    _recentPlatform = (kDevicePlatform)MCR_JDICT_GET_INT(aDict, M_KEY_PLATFORM, kDevicePlatformIOS);
    _createdAt      = MCR_JDICT_GET_DOUBLE(aDict, M_KEY_CREATED_AT, 0.0);
    _updatedAt      = MCR_JDICT_GET_DOUBLE(aDict, M_KEY_UPDATED_AT, 0.0);
    _mergedAt       = MCR_JDICT_GET_DOUBLE(aDict, M_KEY_MERGED_AT, 0.0);
    _isBanned       = MCR_JDICT_GET_INT(aDict, M_KEY_IS_BANNED, 0);
}

void MUserStateResponse::updateFromRefresh(Dictionary* aDict){
    _mergedAt       = MCR_JDICT_GET_DOUBLE(aDict, M_KEY_MERGED_AT, _mergedAt);
}

string MUserStateResponse::getCurrencyValForKey(std::string aKey, string aDefVal){
    Dictionary *dictCurrCurrency = getCurrencyDictForKey(aKey);
    
    if(!dictCurrCurrency){
        return aDefVal;
    }
    
    return MCR_JDICT_GET_STRING(dictCurrCurrency, M_KEY_VALUE, aDefVal);
}

string MUserStateResponse::getScoreValForKey(std::string aKey, string aDefVal){
    if(!getScoreDictForKey(aKey)){
        return aDefVal;
    }
    return MCR_JDICT_GET_STRING(getScoreDictForKey(aKey), M_KEY_VALUE, aDefVal);
}

string MUserStateResponse::getSequenceValForKey(std::string aKey, string aDefVal){
    if(!getSequenceDictForKey(aKey)){
        return aDefVal;
    }
    return MCR_JDICT_GET_STRING(getSequenceDictForKey(aKey), M_KEY_VALUE, aDefVal);
}

void MUserStateResponse::setScoreValueForKey(string aVal,string aKey){
    Dictionary *dictCurrCurrency = getScoreDictForKey(aKey);
    MCR_DICT_ADD(dictCurrCurrency, CCString::create(aVal), M_KEY_VALUE);
    time_t timeToSet = MUtil::getCurrentTime();
    MCR_DICT_ADD(dictCurrCurrency, CCDouble::create(timeToSet), M_KEY_TIME);
}
void MUserStateResponse::setCurrencyValueForKey(string aVal,string aKey){
    Dictionary *dictCurrCurrency = getCurrencyDictForKey(aKey);
    time_t timeToSet = MUtil::getCurrentTime();
    MCR_DICT_ADD(dictCurrCurrency, CCString::create(aVal), M_KEY_VALUE);
    MCR_DICT_ADD(dictCurrCurrency, CCDouble::create(timeToSet), M_KEY_TIME);
}

void MUserStateResponse::setSequenceValueForKey(string aVal,string aKey){
    Dictionary *dictCurrCurrency = getSequenceDictForKey(aKey);
    time_t timeToSet = MUtil::getCurrentTime();
    MCR_DICT_ADD(dictCurrCurrency, CCString::create(aVal), M_KEY_VALUE);
    MCR_DICT_ADD(dictCurrCurrency, CCDouble::create(timeToSet), M_KEY_TIME);
}

Dictionary* MUserStateResponse::getCurrencyDictForKey(std::string aKey){
    Dictionary *cdict = (Dictionary*)MCR_JDICT_GET_OBJECT(_dictCurrencies, aKey.c_str(), NULL);
    return cdict;
}

Dictionary* MUserStateResponse::getScoreDictForKey(std::string aKey){
    return (Dictionary*)MCR_JDICT_GET_OBJECT(_dictScores, aKey.c_str(), NULL);
}

Dictionary* MUserStateResponse::getSequenceDictForKey(std::string aKey){
    return (Dictionary*)MCR_JDICT_GET_OBJECT(_dictSequences, aKey.c_str(), NULL);
}

kDevicePlatform MUserStateResponse::getRecentPlatform(){
    return _recentPlatform;
}

TTime MUserStateResponse::getCreatedAt(){
    return _createdAt;
}

TTime MUserStateResponse::getUpdatedAt(){
    return _updatedAt;
}

TTime MUserStateResponse::getMergedAt(){
    return _mergedAt;
}

bool MUserStateResponse::isBanned(){
    return _isBanned;
}

Dictionary* MUserStateResponse::getDictCurrencies(){
    return _dictCurrencies;
}

Dictionary* MUserStateResponse::getDictScores(){
    return _dictScores;
}

Dictionary* MUserStateResponse::getDictSequences(){
    return _dictSequences;
}
void MUserStateResponse::incrementSeqGlobal(){
    string val = NUM_TO_STR(getSeqGlobal()+1);
    setSequenceValueForKey(val, M_KEY_SEQ_GLOBAL);
}
long MUserStateResponse::getSeqGlobal(){
    return atol(getSequenceValForKey(M_KEY_SEQ_GLOBAL,"0").c_str());
}
NS_CL_END