/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLException.h"

USING_NS_CC;
USING_NS_STD;

NS_CL_BEGIN

Exception::Exception(int aCode, std::string aMessage){
    _message = aMessage;
    _code    = aCode;
}

Exception::Exception(std::string aMessage):Exception(0,aMessage){
    
}

Exception::Exception(int aCode, std::string aMessage, std::string aMsgPrefix){
    _message = aMsgPrefix+" : "+ aMessage;
}

const char* Exception::what()
{
    String *ss = String::createWithFormat("Exception: %s code=(%d)",_message.c_str(),_code);
    _msgToReturn = string(ss->getCString());
    return _msgToReturn.c_str();
}

Exception::~Exception(void){
    
}

NS_CL_END
