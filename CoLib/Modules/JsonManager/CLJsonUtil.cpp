/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLJsonUtil.h"

USING_NS_CC;

NS_CL_BEGIN

Object* JsonUtil::getObject(cocos2d::Dictionary  *aDict, const char*aKey, cocos2d::Object*  aDefObj){
    
    if (!aDict) {
        return aDefObj;
    }
    
    return aDict->objectForKey(aKey);
}

String* JsonUtil::getCCString(cocos2d::Dictionary  *aDict, const char*aKey, cocos2d::String *aDefVal){
    return (String*)JsonUtil::getObject(aDict,aKey,aDefVal);
}

std::string JsonUtil::getString(cocos2d::Dictionary     *aDict, const char*aKey, std::string aDefVal){
    String* obj = JsonUtil::getCCString(aDict,aKey,NULL);
    
    if (!obj) {
        return aDefVal;
    }
    
    return obj->getCString();
}


int JsonUtil::getIntValue(cocos2d::Dictionary   *aDict, const char*aKey, int   aDefVal){
    String* obj = JsonUtil::getCCString(aDict,aKey,NULL);
    
    if (!obj) {
        return aDefVal;
    }
    
    return obj->intValue();
}

float JsonUtil::getFloatValue(cocos2d::Dictionary *aDict, const char*aKey, float aDefVal){
    String* obj = JsonUtil::getCCString(aDict,aKey,NULL);
    
    if (!obj) {
        return aDefVal;
    }
    
    return obj->floatValue();
}

long long JsonUtil::getLongValue(cocos2d::Dictionary   *aDict,const char*aKey, long long   aDefVal){
    String* obj = JsonUtil::getCCString(aDict,aKey,NULL);
    
    if (!obj) {
        return aDefVal;
    }
    
    return obj->doubleValue();
}

double JsonUtil::getDoubleValue(cocos2d::Dictionary *aDict,const char*aKey, long double aDefVal){
    String* obj = JsonUtil::getCCString(aDict,aKey,NULL);
    if (!obj) {
        return aDefVal;
    }
    return obj->doubleValue();
}

bool JsonUtil::getBoolValue(cocos2d::Dictionary  *aDict, const char*aKey, bool  aDefVal){
    return JsonUtil::getIntValue(aDict,aKey,aDefVal);
}

void JsonUtil::setNumberAsStringValue(cocos2d::Dictionary  *aDict, const char*aKey,long aNumber){
    String *newVal = String::createWithFormat("%ld",aNumber);
    MCR_DICT_ADD(aDict, newVal, aKey);
}

void JsonUtil::setNumberAsStringValue(cocos2d::Dictionary  *aDict, const char*aKey,double aNumber){
    String *newVal = String::createWithFormat("%f",aNumber);
    MCR_DICT_ADD(aDict, newVal, aKey);
}


































NS_CL_END