/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MGameObject__
#define COLIB_MGameObject__

#include "CoLibCore.h"

NS_CL_BEGIN

class MLeaf;
class MGameObject : public cocos2d::Object
{
    CC_SYNTHESIZE(TID,_objectId,ObjectId);
    CC_SYNTHESIZE_READONLY(MLeaf*, _metaObject, MetaObject);
    CC_SYNTHESIZE(double, _createdTime, CreatedTime);
    
public:
    virtual bool init();
    virtual void setMetaObject(std::string aLeafKey);
    virtual void setMetaObject(MLeaf *objRef);
    
    virtual cocos2d::Dictionary* toDict() = 0;
    virtual std::string toJson() = 0;
    virtual void loadFromJson(const char* json) = 0;
    virtual void loadFromDict(cocos2d::Dictionary *dict) = 0;
    virtual std::string getTableName() = 0;
    virtual int getObjectType();
};

NS_CL_END
#endif
