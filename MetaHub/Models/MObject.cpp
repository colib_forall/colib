/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MObject.h"
#include "MetaHubCore.h"
#include "MAsset.h"
#include "MContainer.h"

USING_NS_STD;
USING_NS_CC;

NS_CL_BEGIN

char* MObject::_sObjQueryCols = NULL;

string MObject::getQueryColumns(){
    if(!_sObjQueryCols){
        string *queryCols = new string("s_id,"+MCR_CURR_LANG_ALIAS_APPEND_U_SUFFIX("s_name")+",s_sort_order,s_assets");
        _sObjQueryCols = (char*)queryCols->c_str();
    }
    return _sObjQueryCols;
}

TID MObject::getQueryColumnsCount(){
    return 4;
}

MObject::MObject(){
    _id         = 0;
    _name       = "";
    _sortOrder  = 0;
    _dictAssets = NULL;
}
MObject::~MObject(){
    CC_SAFE_RELEASE(_dictAssets);
}

/** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
 */
void MObject::processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){

    int colIndex = 0;
    _id         = SqliteUtil::getIntVal(aRowResult, colIndex++);
    _name       = SqliteUtil::getStringVal(aRowResult, colIndex++, "");
    _sortOrder  = SqliteUtil::getIntVal(aRowResult, colIndex++);
    _dictAssets = new Dictionary();
    
    string strAllAssets = SqliteUtil::getStringVal(aRowResult, colIndex++,"");
    if(not strAllAssets.empty()){
        string assetRecord;
        stringstream resourceInfoCommaStream(assetRecord);
        
        while( getline(resourceInfoCommaStream, assetRecord, M_KEY_DATA_RECORD_SEPERATOR_) ){
            MAsset *  assetObj = new MAsset(assetRecord);
            _dictAssets->setObject(assetObj, assetObj->getID());
            assetObj->release();
        }
    }
    
    M_GLOBAL_DICT->setObject(this, getKey());
}

TID MObject::getId(){
    return _id;
}

string MObject::getName(){
    return _name;
}

int MObject::getSortOrder(){
    return _sortOrder;
}

Dictionary* MObject::getDictAssets(){
    return _dictAssets;
}

string MObject::getPath(){
    return NUM_TO_STR(_id);
}

string MObject::getKey(){
    return NUM_TO_STR(_id);
}

bool MObject::isLocalAsset(TID anAssetID){
    MAsset *assetObj = (MAsset*)_dictAssets->objectForKey(anAssetID);
    if(assetObj){
        return assetObj->isLocal();
    }

    return true; //not found in dictionary so return true
}

TID MObject::getAssetVersion(TID anAssetID){
    MAsset *assetObj = (MAsset*)_dictAssets->objectForKey(anAssetID);
    if(assetObj){
        return assetObj->getVersion();
    }
    return -1;
}

kMAssetType MObject::getAssetType(TID anAssetID)
{
    MAsset *assetObj = (MAsset*)_dictAssets->objectForKey(anAssetID);
    if(assetObj){
        return assetObj->getAssetType();
    }
    
    return kMAssetTypeAny;
}

string MObject::getAssetUDKey(TID anAssetID){
    return getKey()+"_"+NUM_TO_STR(anAssetID);
}

TID MObject::getAssetVersionLocal(TID anAssetID){
    return MCR_UD_GET_INT(getAssetUDKey(anAssetID).c_str());
}

/*returns version of the current asset which id is passed as paramter*/
void MObject::saveAssetVersionLocal(TID anAssetID){
    MCR_UD_SET_INT(getAssetVersion(anAssetID), getAssetUDKey(anAssetID).c_str());
}


/** return primary compulsory asset's path so that shouldUpdateAsset can verify it **/
string MObject::getPrimaryAssetPath(TID anAssetId){
    return "";
}

/** check if particular asset should be updated **/
bool MObject::shouldUpdateAsset(TID anAssetId){
    string primaryAssetPath = getPrimaryAssetPath(anAssetId);
    if (isLocalAsset(anAssetId)) { // asset is local
        if(primaryAssetPath.empty()){
            return false;
        }
        else{
            //check asset's availability in appropriate folder
            string assetCachedPath =  FileUtils::getInstance()->fullPathForFilename(COLIB_LOCAL_ASSET_DIRECTORY+getPath()+"/"+primaryAssetPath);
            if(Util::isFileExistAtPath(assetCachedPath.c_str())){
                return false;
            }
            
            //check asset's availibility in dynamicly downloaded folder
            string assetDynPath = MUtil::getAssetsWriteablePath()+getPath()+"/"+primaryAssetPath;
            if(Util::isFileExistAtPath(assetDynPath.c_str())){
                return false;
            }
        }
    }
    else{//asset is downloaded on requirement
        string assetDynPath = MUtil::getAssetsWriteablePath()+getPath()+"/"+primaryAssetPath;
        if( getAssetVersionLocal(anAssetId) != getAssetVersion(anAssetId) || !Util::isFileExistAtPath(assetDynPath.c_str())){
            return true;
        }
    }
    return false;
}

string MObject::getAssetPath(string aSubPath){
    string path = MUtil::getAssetsWriteablePath()+getPath()+"/"+aSubPath;
    if(Util::isFileExistAtPath(path.c_str())){
        return path;
    }
    else{
        string assetCachedPath =  FileUtils::getInstance()->fullPathForFilename(COLIB_LOCAL_ASSET_DIRECTORY+getPath()+"/"+aSubPath);
        if(Util::isFileExistAtPath(assetCachedPath.c_str())){
            return assetCachedPath;
        }
    }
    
    string nullImgPath = FileUtils::getInstance()->fullPathForFilename(COLIB_NULL_IMAGE);
    return nullImgPath;
}

NS_CL_END

