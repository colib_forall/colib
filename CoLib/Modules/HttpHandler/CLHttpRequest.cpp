/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#define _CO_HTTP_REQUEST_DEFAULT_TIMEOUT    30
#define _CO_HTTP_CONNECTION_TIMEOUT         30

#include "CLHttpRequest.h"

NS_CL_BEGIN

#pragma mark Protected methods
HttpRequest::HttpRequest()
{
    _requestType    =   kRequestTypeFetcher;
    _httpReqType    =   kHttpRequestTypeGet;
    _tag            =   0;
    _params         =   NULL;
    _headers        =   NULL;
}
HttpRequest::HttpRequest(kRequestType aRequestType, string aURL,int aTag, Dictionary *theParams,kHttpRequestType reqType)
{
    _requestType    =   aRequestType;
    _httpReqType    =   reqType;
    _tag            =   aTag;
    _uRL            =   aURL;
    _params         =   theParams;
    CC_SAFE_RETAIN(_params);
    _timeOutConnection = TIMEOUT_CONNECTION_DEFAULT;
    _timeOutRequest    = TIMEOUT_REQUEST_DEFAULT;
    _headers           = NULL;
}
HttpRequest::HttpRequest(string aURL,int aTag, Dictionary *theParams, string aFilePath, string aFileName,kHttpRequestType reqType)
{
    _filePath   =   aFilePath;
    _fileName   =   aFileName;
    _requestType    =   kRequestTypeDownloader;
    _httpReqType    =   reqType;
    _tag            =   aTag;
    _uRL            =   aURL;
    _params         =   theParams;
    CC_SAFE_RETAIN(_params);
    _timeOutConnection = TIMEOUT_CONNECTION_DEFAULT;
    _timeOutRequest    = TIMEOUT_REQUEST_DEFAULT;
    _headers        = NULL;
}
HttpRequest::HttpRequest(string aURL,int aTag, Dictionary* theParams,kHttpRequestType reqType) // request fetcher
{
    _requestType    =   kRequestTypeFetcher;
    _httpReqType    =   reqType;
    _tag            =   aTag;
    _uRL            =   aURL;
    _params         =   theParams;
    CC_SAFE_RETAIN(_params);
    _timeOutConnection = TIMEOUT_CONNECTION_DEFAULT;
    _timeOutRequest    = TIMEOUT_REQUEST_DEFAULT;
    _headers        = NULL;
}
#pragma mark Public methods
// static method for request downloader
HttpRequest* HttpRequest::create(string aURL,int aTag, Dictionary *theParams, string aFilePath, string aFileName,kHttpRequestType reqType)
{
    //validate all parameters
    if(HttpRequest::validate(aURL) == false || HttpRequest::validate(aFilePath, aFileName) == false)
    {
        return NULL;
    }
    
    HttpRequest *requestDownloader = new HttpRequest(aURL,aTag,theParams,aFilePath,aFileName,reqType);
    requestDownloader->autorelease();
    return requestDownloader;
}
HttpRequest* HttpRequest::create(string aURL,int aTag, Dictionary *theParams,Dictionary *fileParams,string aFilePath, string aFileName,kHttpRequestType reqType)
{
    //validate all parameters
    if(HttpRequest::validate(aURL) == false || HttpRequest::validate(aFilePath, aFileName) == false)
    {
        return NULL;
    }
    if(fileParams && fileParams->count() > 0)
    {
        theParams->setObject(fileParams, KEY_FILE_PARAMS);
    }
    HttpRequest *requestDownloader = new HttpRequest(aURL,aTag,theParams,aFilePath,aFileName,reqType);
    requestDownloader->autorelease();
    return requestDownloader;
}
// static method for request fetcher
HttpRequest* HttpRequest::create(string aURL,int aTag, Dictionary *theParams,kHttpRequestType reqType)
{
    if(HttpRequest::validate(aURL) == false)
    {
        return NULL;
    }
    HttpRequest *requestFetcher = new HttpRequest(aURL,aTag,theParams,reqType);
    requestFetcher->autorelease();
    return requestFetcher;
}
HttpRequest* HttpRequest::create(string aURL,int aTag, Dictionary *theParams,Dictionary *fileParams,kHttpRequestType reqType)
{
    if(HttpRequest::validate(aURL) == false)
    {
        return NULL;
    }
    if(fileParams && fileParams->count() > 0)
    {
        theParams->setObject(fileParams, KEY_FILE_PARAMS);
    }
    HttpRequest *requestFetcher = new HttpRequest(aURL,aTag,theParams,reqType);
    requestFetcher->autorelease();
    return requestFetcher;
}
kRequestType HttpRequest::getRequestType()
{
    return _requestType;
}
int HttpRequest::getTag()
{
    return _tag;
}
string HttpRequest::getURL()
{
    return _uRL;
}
Dictionary* HttpRequest::getParams()
{
    return _params;
}
int HttpRequest::getTimeoutRequest()
{
    return _timeOutRequest;
}
int HttpRequest::getTimeoutConnection()
{
    return _timeOutConnection;
}
kHttpRequestType HttpRequest::getHttpRequestType()
{
    return _httpReqType;
}
void HttpRequest::setURL(string aURL)
{
    _uRL = aURL;
}
void HttpRequest::setCCDictionary(Dictionary *aParams)
{
    CC_SAFE_RELEASE_NULL(_params);
    _params = aParams;
    CC_SAFE_RETAIN(_params);
}
void HttpRequest::setTimeoutRequest(int aTimeoutRequest)
{
    _timeOutRequest = aTimeoutRequest;
}
void HttpRequest::setTimeoutConnection(int aTimeoutConnection)
{
    _timeOutConnection = aTimeoutConnection;
}
void HttpRequest::setCustomHeaders(Dictionary*)
{
    //set custom headers here
}
void HttpRequest::setHttpRequestType(kHttpRequestType type)
{
    _httpReqType = type;
}
void HttpRequest::setPostBody(string aPostBody)
{
    _postBody = aPostBody;
}
void HttpRequest::setHeaders(cocos2d::Dictionary *headers)
{
    CC_SAFE_RETAIN(headers);
    CC_SAFE_RELEASE_NULL(_headers);
    _headers = headers;
}
#pragma mark getter/setters
string HttpRequest::getFilePath()
{
    return _filePath;
}
string HttpRequest::getFileName()
{
    return _fileName;
}
string HttpRequest::getFullPath()
{
    return _filePath + _fileName;
}
string HttpRequest::getTempFilePath()
{
    return _filePath + _fileName + ".temp";
}
void HttpRequest::setFilePath(string aFilePath)
{
    _filePath = aFilePath;
}
void HttpRequest::setFileName(string aFileName)
{
    _fileName = aFileName;
}
string HttpRequest::getPostBody()
{
    return _postBody;
}
cocos2d::Dictionary*  HttpRequest::getHeaders()
{
    return _headers;
}
HttpRequest::~HttpRequest()
{
    CC_SAFE_RELEASE(_params);
    CC_SAFE_RELEASE(_headers);
}

bool HttpRequest::validate(string aUrl)
{
    return (aUrl.empty()) ? false : true;
}
bool HttpRequest::validate(string aFilePath, string aFileName)
{
    return (aFileName.empty() || aFilePath.empty()) ? false : true;
}
NS_CL_END
