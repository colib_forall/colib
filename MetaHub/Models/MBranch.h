/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MBranch__
#define COLIB_MBranch__

#include "MObject.h"


NS_CL_BEGIN

class MTree;
class MLeaf;
class MCLeaf;

class MBranch : public MObject
{
    friend MLeaf;
    
private:
    static char *_sBranchQueryCols;
    
    MTree       *_refTree;  //to store the reference of parent Tree
    Dictionary  *_dictLeaves;
    
    
    kLeafType   _leafType;  // to distinguish the Leaf Type, Complex or Simple
    
    void addLeaf(MLeaf *aLeaf);
    
public:
    
    MBranch();
    virtual ~MBranch();
    
    /**returns the tree referecen of current MBranch*/
    MTree*      getRefTree();
    /**returns the leaf type complex or simple*/
    kLeafType   getLeafType();
    
    /** return all the leaves **/
    Dictionary* getLeaves();

    /** returns all the leaves for user's applicable level **/
    Dictionary* getLeaves(bool considerVisible,bool considerAppVersion,int aStartLevel, int anEndLevel);
    
    /** returns the Column relavant to current object, concats the parents col via super*/
    static std::string getQueryColumns();
    
    /** returns the colmn count of current MObject*/
    static TID getQueryColumnsCount();

    /**returns a relative path for getting the assets folder*/
    virtual string getPath();
    
    /**returns the string represenation of the key fro direct access of MObject from Map*/
    virtual string getKey();
    
    /** virtual method, any class using SqliteController must implement this method , i.e Branch is Child of Tree and Leaf is Child of Branch
     */
    virtual void executeChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult,void* aCustomObj);
    
    /** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
     */
    virtual void processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj);
    
    /** return the child query
     */
    virtual std::string getChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj);
    
    cocos2d::Array *getSortedLeafs();
    cocos2d::Array *getSortedLeafs(bool considerVisible,bool considerAppVersion,int aStartLevel, int anEndLevel);
};

NS_CL_END

#endif /* defined(COLIB_MBranch__) */
