/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MObject__
#define COLIB_MObject__

#include "CoLibCore.h"
#include "CoLibSqlite.h"
#include "MetaHubCore.h"

USING_NS_CC;
USING_NS_STD;

NS_CL_BEGIN

class MObject : public cocos2d::Object, public SqliteControllerProtocol
{
private:
    static char *_sObjQueryCols;
protected:
    TID         _id;
    string      _name;
    int         _sortOrder;
    Dictionary  *_dictAssets;


public:
    MObject();
    virtual ~MObject();
    
    /**returns the id of curretn Mobject*/
    TID         getId();
    
    /**returns the name of the Mobject*/
    string      getName();

    /**returns the sort order of the MObject*/
    int         getSortOrder();
    
    /**returns the assets dictionary a MObject has*/
    Dictionary* getDictAssets();
    
    /**returns true if asset is part of the project i.e pre cached according to MObject getPath()*/
    bool isLocalAsset(TID anAssetID);
    
    /*returns version of the current asset which id is passed as paramter*/
    TID getAssetVersion(TID anAssetID);
    
    /*Functions for CC Defaults*/
    string getAssetUDKey(TID anAssetID);
    TID getAssetVersionLocal(TID anAssetID);
    void saveAssetVersionLocal(TID anAssetID);
    
    /** returns the asset path **/
    std::string getAssetPath(string aSubPath);

    /*returns the type of the current asset which id is passed as paramter*/
    kMAssetType getAssetType(TID anAssetID);
    
    /** check if particular asset should be updated **/
    virtual bool shouldUpdateAsset(TID anAssetId);
    
    /** return primary compulsory asset's path so that shouldUpdateAsset can verify it **/
    virtual string getPrimaryAssetPath(TID anAssetId);
    
    /**returns a relative path for getting the assets folder*/
    virtual string getPath();
    
    /**returns the string represenation of the key fro direct access of MObject from Map*/
    virtual string getKey();
    
    /** returns the Column relavant to current object, concats the parents col via super*/
    static std::string getQueryColumns();
    
    /** returns the colmn count of current MObject*/
    static TID getQueryColumnsCount();
    
    /** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
     */
    virtual void processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj);
    
};

NS_CL_END
#endif /* defined(COLIB_MObject__) */
