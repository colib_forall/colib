/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLHttpHandler.h"
#include "CLHttpRequest.h"

NS_CL_BEGIN
#define NETWORK_DELAY_FOR_THREAD .1

#pragma mark Private method
//methods
HttpHandler::HttpHandler(HttpHandlerProtocol *aDelegate, HttpRequest *aRequest)
{
    _delegate = aDelegate;
    _request  = aRequest;
    
    _handlerState = kHandlerStateInitial;
    _request->retain();
    
    _pthread    = NULL;
    _response   = NULL;
    
    _shouldCancel = false;
    
    _headerData.clear();
    _responseData.clear();
    _fileStream.clear();
    _headerDict = NULL;
    
    _response = new HttpResponse(this, _request);
    
    curl_global_init(CURL_GLOBAL_ALL);
}
HttpHandler::~HttpHandler()
{
    CCLOG("~HttpHandler()");
    _headerData.clear();
    _responseData.clear();
    _fileStream.clear();
    CC_SAFE_RELEASE(_request);
    CC_SAFE_RELEASE(_response);
    CC_SAFE_RELEASE(_headerDict);
}
bool HttpHandler::setCURLCommons(CURL *handle)
{
    if (!handle) {
        return false;
    }
    
    int32_t code;
    code = curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, _errorBuffer);
    if (code != CURLE_OK) {
        return false;
    }
    
    code = curl_easy_setopt(handle, CURLOPT_TIMEOUT, _request->getTimeoutRequest());
    
    if (code != CURLE_OK) {
        return false;
    }
    
    code = curl_easy_setopt(handle, CURLOPT_CONNECTTIMEOUT, _request->getTimeoutConnection());
    if (code != CURLE_OK) {
        return false;
    }
    return true;
}
int HttpHandler::treatGetRequest()
{
    //sleep(10);
    //return 0;
    
    CCLOG("<<tCurl started for thread %i >> \n", _request->getTag());
    CURLcode curlCode = CURL_LAST;
    _curl = curl_easy_init();
    curl_slist *curlHeaders = NULL;
    bool  requestProperlySettedUp = true;
    
    if(_request->getRequestType()==kRequestTypeDownloader)
    {
        _fileStream.open(_request->getTempFilePath().data());
    }
    do {
        CCLOG("Request Tag = %d", _request->getTag());
        if (!setCURLCommons(_curl))
        {
            requestProperlySettedUp = false;
            break;
        }
        curlCode = setRequestHeaders(curlHeaders);
        if(curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        
        string url = _request->getURL();
        Dictionary *params = _request->getParams();
        if(params && params->count() > 0)
        {
            Array *allKeys = params->allKeys();
            stringstream str;
            for(int i=0; i<allKeys->count(); i++)
            {
                String *aKey = (String*)allKeys->getObjectAtIndex(i);
                String *aValue = (String*)params->objectForKey(aKey->getCString());
                str << aKey->getCString() << "=" << aValue->getCString();
            }
            //const char *finalParam = curl_escape(str.str().c_str(), str.str().length());
            string finalParam= Util::stringEscape(str.str().c_str());
            
            std::stringstream finalURL;
            finalURL << url << "?" << finalParam;
            url = finalURL.str();
            CCLOG("Final URL is %s",url.c_str());
        }
        curlCode = curl_easy_setopt(_curl, CURLOPT_URL, url.data());
        
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
//        if(_request->getPostBody().size() > 0)
//        {
//            curlCode = curl_easy_setopt(_curl, CURLOPT_POSTFIELDS, _request->getPostBody().c_str());
//            if(curlCode != CURLE_OK)
//            {
//                requestProperlySettedUp = false;
//                break;
//            }
//            curlCode = curl_easy_setopt(_curl, CURLOPT_POSTFIELDSIZE, _request->getPostBody().size());
//            if(curlCode != CURLE_OK)
//            {
//                requestProperlySettedUp = false;
//                break;
//            }
//        }
        curlCode = curl_easy_setopt(_curl, CURLOPT_NOSIGNAL, 1);
        
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        
        curlCode = curl_easy_setopt(_curl, CURLOPT_FOLLOWLOCATION, _request->getURL().data());
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        
        curlCode = curl_easy_setopt(_curl, CURLOPT_HEADERDATA, this);
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        
        curl_easy_setopt(_curl, CURLOPT_HEADERFUNCTION, HttpHandler::headerHandler);
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        
        curlCode = curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, HttpHandler::responseHandler);
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        
        curlCode = curl_easy_setopt(_curl, CURLOPT_WRITEDATA, this);
        if (curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }

        curlCode = curl_easy_perform(_curl);
        if (curlCode != CURLE_OK)
        {
            break;
        }
        
        int curResponseCode = 0;
        curlCode = curl_easy_getinfo(_curl, CURLINFO_RESPONSE_CODE, &curResponseCode);
        
        if (curlCode != CURLE_OK || curResponseCode != 200)
        {
            curlCode = CURLE_HTTP_RETURNED_ERROR;
        }
        
    } while (0);
    
    if(_curl)
    {
        curl_easy_cleanup(_curl);
    }
    /* free the linked list for header data */
    if (curlHeaders)
    {
        curl_slist_free_all(curlHeaders);
    }
    if(curlCode == CURLE_OK)
    {
        saveFileData();
    }
    else if (_fileStream)
    {
        _fileStream.close();
    }
    parseResponseHeader();
    _curl = NULL;
    return (curlCode == CURLE_OK ? 0 : 1);
}
int HttpHandler::treatPostRequest()
{
    bool  requestProperlySettedUp = true;
    CURLcode curlCode = CURL_LAST;
    CURLM *multi_handle = NULL;
    int still_running;
    
    struct curl_httppost *formpost=NULL;
    struct curl_httppost *lastptr=NULL;
    curl_slist *curlHeaders = NULL;
    
    do
    {
        if(_request->getRequestType() == kRequestTypeDownloader)
        {
            std::string path = _request->getTempFilePath();
            _fileStream.open(path.data());
        }
        /* Fill in the file upload field. This makes libcurl load data from
         the given file name when curl_easy_perform() is called. */
        Dictionary *params = _request->getParams();
        if(params && params->count() > 0)
        {
            Array *allKeys = params->allKeys();
            for(int i=0; i<allKeys->count(); i++)
            {
                String *aKey = (String*)allKeys->getObjectAtIndex(i);
                if(aKey->compare(KEY_FILE_PARAMS) == 0)
                {
                    Dictionary *files = (Dictionary*)params->objectForKey(aKey->getCString());
                    Array *fileKeys = files->allKeys();
                    for(int j = 0; j < fileKeys->count() ; j++)
                    {
                        aKey = (String*)fileKeys->getObjectAtIndex(j);
                        String *filePath = (String*)files->objectForKey(aKey->getCString());
                        CURLFORMcode code = curl_formadd(&formpost,
                                     &lastptr,
                                     CURLFORM_COPYNAME, aKey->getCString(),
                                     CURLFORM_FILE, filePath->getCString(),
                                     CURLFORM_END);
                        if(code == CURL_FORMADD_OK)
                            curlCode = CURLE_OK;
                        else
                        {
                            curlCode = CURL_LAST;
                            break;
                        }
                        curl_formadd(&formpost,
                                     &lastptr,
                                     CURLFORM_COPYNAME, "filename",
                                     CURLFORM_COPYCONTENTS, filePath->getCString(),
                                     CURLFORM_END);
                    }
                }
                else
                {
                    String *aValue = (String*)params->objectForKey(aKey->getCString());
                    CURLFORMcode code = curl_formadd(&formpost,
                                 &lastptr,
                                 CURLFORM_COPYNAME, aKey->getCString(),
                                 CURLFORM_PTRCONTENTS, aValue->getCString(),
                                 CURLFORM_END);
                    if(code == CURL_FORMADD_OK)
                    {
                        curlCode = CURLE_OK;
                    }
                    else
                    {
                        curlCode = CURL_LAST;
                        break;
                    }
                }
            }
            if(curlCode != CURLE_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            
            CURLFORMcode code = curl_formadd(&formpost,
                         &lastptr,
                         CURLFORM_COPYNAME, "submit",
                         CURLFORM_COPYCONTENTS, "send",
                         CURLFORM_END);
            if(code != CURL_FORMADD_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
        }
        _curl = curl_easy_init();
        if (!setCURLCommons(_curl))
        {
            requestProperlySettedUp = false;
            break;
        }
        curlCode = setRequestHeaders(curlHeaders);
        if(curlCode != CURLE_OK)
        {
            requestProperlySettedUp = false;
            break;
        }
        multi_handle = curl_multi_init();
        
        /* initalize custom header list (stating that Expect: 100-continue is not
         wanted */
        if(_curl && multi_handle)
        {
            /* what URL that receives this POST */
            string url = _request->getURL();
            curlCode = curl_easy_setopt(_curl, CURLOPT_URL, url.data());
            if(curlCode != CURLE_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            curlCode = curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, HttpHandler::responseHandler);
            if(curlCode != CURLE_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            curlCode = curl_easy_setopt(_curl, CURLOPT_WRITEDATA, this);
            if(curlCode != CURLE_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            curlCode =  curl_easy_setopt(_curl, CURLOPT_VERBOSE, 1L);
            if(curlCode != CURLE_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            curlCode = curl_easy_setopt(_curl, CURLOPT_HTTPPOST, formpost);
            if(curlCode != CURLE_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            if(_request->getPostBody().size() > 0)
            {
                curlCode = curl_easy_setopt(_curl, CURLOPT_POSTFIELDS, _request->getPostBody().c_str());
                if(curlCode != CURLE_OK)
                {
                    requestProperlySettedUp = false;
                    break;
                }
                curlCode = curl_easy_setopt(_curl, CURLOPT_POSTFIELDSIZE, _request->getPostBody().size());
                if(curlCode != CURLE_OK)
                {
                    requestProperlySettedUp = false;
                    break;
                }
            }
            CURLMcode code = curl_multi_add_handle(multi_handle, _curl);
            if(code != CURLM_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            code = curl_multi_perform(multi_handle, &still_running);
            if(code != CURLM_OK)
            {
                requestProperlySettedUp = false;
                break;
            }
            do {
                struct timeval timeout;
                int rc; /* select() return code */
                
                fd_set fdread;
                fd_set fdwrite;
                fd_set fdexcep;
                int maxfd = -1;
                
                long curl_timeo = -1;
                
                FD_ZERO(&fdread);
                FD_ZERO(&fdwrite);
                FD_ZERO(&fdexcep);
                
                /* set a suitable timeout to play around with */
                timeout.tv_sec = 1;
                timeout.tv_usec = 0;
                
                curl_multi_timeout(multi_handle, &curl_timeo);
                if(curl_timeo >= 0) {
                    timeout.tv_sec = curl_timeo / 1000;
                    if(timeout.tv_sec > 1)
                        timeout.tv_sec = 1;
                    else
                        timeout.tv_usec = (curl_timeo % 1000) * 1000;
                }
                
                /* get file descriptors from the transfers */
                curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);
                
                /* In a real-world program you OF COURSE check the return code of the
                 function calls.  On success, the value of maxfd is guaranteed to be
                 greater or equal than -1.  We call select(maxfd + 1, ...), specially in
                 case of (maxfd == -1), we call select(0, ...), which is basically equal
                 to sleep. */
                
                rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);
                
                switch(rc) {
                    case -1:
                        /* select error */
                        break;
                    case 0:
                    default:
                        /* timeout or readable/writable sockets */
                        //                    printf("perform!\n");
                        curl_multi_perform(multi_handle, &still_running);
                        //                  printf("running: %d!\n", still_running);
                        break;
                }
            } while(still_running);
        }
    }while (0);
    
//        int curResponseCode = 0;
//        curlCode = curl_easy_getinfo(_curl, CURLINFO_RESPONSE_CODE, &curResponseCode);
//        if (curlCode != CURLE_OK || curResponseCode != 200)
//        {
//            curlCode = CURLE_HTTP_RETURNED_ERROR;
//        }
        
        if(multi_handle)
        {
            curl_multi_cleanup(multi_handle);
        }
        
        if(_curl)
        {
            /* always cleanup */
            curl_easy_cleanup(_curl);
        }
        
        if(formpost)
        {
            /* then cleanup the formpost chain */
            curl_formfree(formpost);
        }
        
        /* free the linked list for header data */
        if (curlHeaders)
        {
            curl_slist_free_all(curlHeaders);
        }
        if(curlCode == CURLE_OK)
        {
            saveFileData();
        }
        else if (_fileStream)
        {
            _fileStream.close();
        }
        parseResponseHeader();
    
    return (requestProperlySettedUp == true ? 0 : 1);
}
void HttpHandler::handleFailed()
{
    if (!_delegate)
    {
        return;
    }
    Director::getInstance()->getScheduler()->scheduleSelector(schedule_selector(HttpHandler::handleFailedTask), this, 0.0f, 0.0f, NETWORK_DELAY_FOR_THREAD , false);
    
    _handlerState = kHandlerStateCompleted;
    //terminateThread();
    _pthread = NULL;
}
void HttpHandler::handleFailedTask(float dt)
{
    if(!_delegate) return;
    _delegate->responseFailed(_response,_statusCode); // change the null
}
void HttpHandler::handleResponseSuccess()
{
    if(!_delegate) return;
    
    //conditions here if final make Response Object
    CCLOG("Data Arrived now, ready to validate response... %d ", _request->getTag());
    
    if(_delegate->responseAuth(_response))
    {
        Director::getInstance()->getScheduler()->scheduleSelector(schedule_selector(HttpHandler::handleSuccessTask), this, 0.0f, 0.0f, NETWORK_DELAY_FOR_THREAD , false);
        
        _handlerState = kHandlerStateCompleted;
        CCLOG("Calling success for thread ... %d ", _request->getTag());
        
        //terminateThread();
        _pthread = NULL;
    }
    else
    {
        _statusCode = kResponseStatusCodeAuthFailed;
        handleFailed();
    }
}
void HttpHandler::handleSuccessTask(float dt)
{
    if(!_delegate) return;
    _delegate->responseSuccess(_response);
}
void HttpHandler::prcessResponseData()
{
    //basic authentication checks
}

bool HttpHandler::createThread(void* anInstance)
{
    if(_pthread)
    {
        CCLOG("already exists");
        return true;
    }
    
    //pthread_t pthread_;
    int ret = pthread_create(&_pthread, NULL, &HttpHandler::startThread, anInstance);
    //pthread_detach(*pthread_);
    
    if(ret == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void HttpHandler::preProcessThread()
{
    int curlCode = CURL_LAST;
    if(_request->getHttpRequestType() == kHttpRequestTypeGet)
    {
        curlCode = treatGetRequest();
    }
    else
    {
        curlCode = treatPostRequest();
    }
    if(curlCode != CURLE_OK)
    {
        _statusCode = kResponseStatusCodeFailed;
        handleFailed();
    }
    else
    {
        handleResponseSuccess();
    }
}
void HttpHandler::terminateThread()
{
    if(_shouldCancel && _handlerState == kHandlerStateInProcess)
    {
        int s = 0;
        
        //s = pthread_cancel(pthread_);
        //s = pthread_setcancelstate(3);
        //
        //s = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
        s = pthread_cancel(_pthread);
        
        pthread_detach(_pthread);
        
        //sleep(100);
        
        if (s != 0)
            CCLOG("\n\n\n**************critical , thread not canceld********************** %d \n\n", s);
        else
            CCLOG("\n\n\n******************thread canceled with tag %i******************** \n\n\n",_request->getTag());
        
    }
    else
    {
        CCLOG("\n\nTERMINATE SUCCESS TAG (%i) with value of shouldCancel_: %i   handlerState: %i \n\n\n",_request->getTag(), _shouldCancel,_handlerState);
    }
    
    //    if(pthread_kill(pthread_, 0) == 0)
    //    {
    //        CCLog("*******   \\(^-^)//  **********   main to zinda hun :P with tag %i ", request_->getTag());
    //    }
    
    _pthread = NULL;
}
void HttpHandler::terminateThreadTask()
{
    
}
CURLcode HttpHandler::setRequestHeaders(curl_slist *curlHeaders)
{
    /* get custom header data (if set) */
    cocos2d::Dictionary* headers = _request->getHeaders();
    if(headers && headers->count() > 0)
    {
        DictElement* pElement = NULL;
        CCDICT_FOREACH(headers, pElement)
        {
            stringstream strStream;
            cocos2d::String *headerVal = (cocos2d::String*)pElement->getObject();
            strStream << pElement->getStrKey() << ": " << headerVal->getCString();
            curl_slist_append(curlHeaders,strStream.str().c_str());
            strStream.str() = "";
            strStream.clear();
        }
        /* set custom headers for curl */
        return  curl_easy_setopt(_curl, CURLOPT_HTTPHEADER, curlHeaders);
    }
    return CURLE_OK;
}
void HttpHandler::parseResponseHeader()
{
    if(_headerData.size() > 0)
    {
        std::string headers(_headerData.begin(),_headerData.end());
        CCLOG("response headers are %s",headers.c_str());
        std::stringstream ss(headers);
        std::string line;
        _headerDict = new Dictionary();
        while (std::getline(ss, line))
        {
            std::cout << line << std::endl;
            std::size_t found = line.find(":");
            if (found != std::string::npos)
            {
                std::string key = line.substr(0,found);
                std::string val = line.substr(found+2);
                int slashPos = val.find_last_of("\r");
                if(slashPos != std::string::npos)
                {
                    val = val.substr(0,slashPos);
                }
                CCLOG("key = %s, value = %s",key.c_str(),val.c_str());
                String* str = new String(val);
                _headerDict->setObject(str, key.c_str());
                str->release();
            }
        }
        _headerData.clear(); // free it as data is now stored in '_headerDict'
    }
}
void HttpHandler::saveFileData()
{
    if(_request->getRequestType() == kRequestTypeDownloader && _fileStream)
    {
        _fileStream.close();
        string oldName = _request->getTempFilePath();
        string newName = _request->getFullPath();
        int result = rename( oldName.c_str() , newName.c_str() );
        if ( result == 0 )
        {
            CCLOG( "File successfully renamed" );
        }
        else
        {
            CCLOG( "Error renaming file" );
        }
    }
}
#pragma mark static function for thread
void* HttpHandler::startThread(void* obj)
{
    HttpHandler *hDPtr = (HttpHandler*) obj;
    CCLOG("<<thread started %i >> \n", hDPtr->_request->getTag());
    hDPtr->preProcessThread();
    return hDPtr;
}
#pragma mark curl Callback methods
size_t HttpHandler::headerHandler(void *ptr, size_t size,
                                  size_t nmemb, void *stream)
{
    size_t sizes = size * nmemb;
    HttpHandler *currHandler = (HttpHandler*) stream; // this point of the current object
    if(currHandler->_shouldCancel)
    {
        return 0; // break here.
    }
    
    std::vector<char> *recvBuffer = (std::vector<char>*)&currHandler->_headerData;
    recvBuffer->insert(recvBuffer->end(), (char*)ptr, (char*)ptr+sizes);
    return sizes;
}

size_t HttpHandler::responseHandler(void *ptr, size_t size,
                                    size_t nmemb, void *stream)
{
    size_t sizes = size * nmemb;
    
    HttpHandler *currHandler = (HttpHandler*) stream; // this point of the current object
    
    if(currHandler->_shouldCancel)
    {
        return 0; // break here.
    }
    
    if(currHandler->_request->getRequestType() == kRequestTypeFetcher)
    {
        std::vector<char> *recvBuffer = (std::vector<char>*)&currHandler->_responseData;
        recvBuffer->insert(recvBuffer->end(), (char*)ptr, (char*)ptr+sizes);
    }
    else if(currHandler->_request->getRequestType() == kRequestTypeDownloader)
    {
        currHandler->_fileStream.write((char*)ptr, sizes);
    }
    else
    {
        currHandler->handleFailed();
    }
    
    return sizes;
}
#pragma mark Public methods
HttpHandler* HttpHandler::create(HttpHandlerProtocol *aDelegate, HttpRequest *aRequest)
{
    //do verification checks
    if(!aDelegate || !aRequest)
    {
        return NULL;
    }
    
    HttpHandler *handlerObj = new HttpHandler(aDelegate,aRequest);
    handlerObj->autorelease();
    return handlerObj;
}
void HttpHandler::start()
{
    if(_shouldCancel) // this requested is not valid so do nothing
    {
        return;
    }
    CCLOGINFO("<start the thread %d>",_request->getTag());
    
    if(_handlerState == kHandlerStateInProcess)
    {
        CCLOGINFO("instance for tag id %d already in progress ",_request->getTag());
    }
    else
    {
        _handlerState = kHandlerStateInProcess;
        createThread(this);
    }
}
void HttpHandler::invalidate()
{
    if(!_delegate)
    {
        return;
    }
    
    CCLOGINFO("Network Manager Invalidate, with tag %d", _request->getTag());
    
    _delegate = NULL;
    _shouldCancel = true;
    terminateThread();
}

void HttpHandler::updateRequest(HttpRequest* aRequest,HttpHandlerProtocol *aDelegate)
{
    
}

kHandlerState HttpHandler::getHandlerState(){
    return _handlerState;
}

kResponseStatusCode HttpHandler::getStatusCode()
{
    return _statusCode;
}
char* HttpHandler::getResponseData()
{
    return _responseData.size() > 0 ? _responseData.data() : NULL;
}
void HttpHandler::setTag(int tag)
{
    _tag = tag;
}
int HttpHandler::getTag()
{
    return _tag;
}
cocos2d::Dictionary* HttpHandler::getHeaderData()
{
    return _headerDict;
}
NS_CL_END