/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLUtil.h"
#include "../cocos2dx/support/zip_support/unzip.h"

USING_NS_CC;

NS_CL_BEGIN

/** Converts any int to string
 */
string Util::numberToString(int aNumber){
    return std::to_string(aNumber);
//    stringstream ss;//create a stringstream
//    ss << aNumber;//add number to the stream
//    return ss.str();//return a string with the contents of the stream
}


/** Converts any TID to string
 */
string Util::numberToString(TID aNumber){
    return std::to_string(aNumber);
//    stringstream ss;//create a stringstream
//    ss << aNumber;//add number to the stream
//    return ss.str();//return a string with the contents of the stream
}

/** Converts any descreat number to string
 */
string Util::numberToString(long aNumber){
    return std::to_string(aNumber);
//    stringstream ss;//create a stringstream
//    ss << aNumber;//add number to the stream
//    return ss.str();//return a string with the contents of the stream
}

/** Converts any descreat number to string
 */
string Util::numberToString(long long aNumber){
    return std::to_string(aNumber);
    //    stringstream ss;//create a stringstream
    //    ss << aNumber;//add number to the stream
    //    return ss.str();//return a string with the contents of the stream
}


/** Converts any float to string
 */
string Util::numberToString(double aDouble){
    return std::to_string(aDouble);
//    stringstream ss;//create a stringstream
//    ss << aDouble;//add number to the stream
//    return ss.str();//return a string with the contents of the stream
}

kDevicePlatform Util::getDevicePlatform(){
    
    switch (CURR_PLATFORM) {
        case ApplicationProtocol::Platform::OS_ANDROID:
            return kDevicePlatformAndroid;
            break;
        case ApplicationProtocol::Platform::OS_IPAD:
            return kDevicePlatformIOS;
            break;
        case ApplicationProtocol::Platform::OS_IPHONE:
            return kDevicePlatformIOS;
            break;
        default:
            return kDevicePlatformUnknown;
    }
}

#warning dummy implementation for function, write native implementation for these.

string Util::getOSVersion(){
    return "5.0";
}

string Util::getDeviceModel(){
    return "Apple+iPOD";
}

float Util::getBundledVersion(){
    return 1.0;
}

float Util::getBuildNumber(){
    return 1.0;
}

string Util::getLanguageAlias(cocos2d::LanguageType langType){
    
    switch (langType) {
        
        case LanguageType::ENGLISH:
            return ""; //default case
            break;
            
        case LanguageType::CHINESE:
            return "zh";
            break;

        case LanguageType::FRENCH:
            return "fr";
            break;
        case LanguageType::GERMAN:
            return "de";
            break;
        case LanguageType::SPANISH:
            return "es";
            break;
        case LanguageType::RUSSIAN:
            return "ru";
            break;
        case LanguageType::KOREAN:
            return "ko";
            break;
        case LanguageType::JAPANESE:
            return "ja";
            break;
        case LanguageType::HUNGARIAN:
            return "hu";
            break;
        case LanguageType::PORTUGUESE:
            return "pt";
            break;
        case LanguageType::ARABIC:
            return "ar";
            break;
        case LanguageType::NORWEGIAN:
            return "nb";
            break;
        case LanguageType::POLISH:
            return "pl";
            break;
        default:
            return "";
            break;
    }
}

bool Util::isFileExistAtPath(const char *aFilePath){
    return CCFileUtils::getInstance()->isFileExist(aFilePath);
}

string Util::getDelimiteredString(long long aNumb,int delmiterLoc, const char *aDelimiter)
{
    bool isNegative= (aNumb < 0) ? true : false;
    aNumb = llabs(aNumb);
    
    string strRep = NUM_TO_STR(aNumb);
    
    if(delmiterLoc <=0) return strRep;
    
    int strLength = strRep.length(),noOfCommas = strLength/delmiterLoc;
    int remainder = strLength%delmiterLoc, inc = 1;
    if (remainder == 0) {
        noOfCommas--;
    }
    string finalStr = "";
    for (; inc <= noOfCommas; inc++) {
        finalStr = aDelimiter+strRep.substr(strLength - inc*delmiterLoc,delmiterLoc) + finalStr;
    }
    finalStr = strRep.substr(0,(remainder == 0) ? delmiterLoc : remainder) + finalStr;
    
    return ((isNegative) ? "-" : "") + finalStr;
}

string Util::getShortenedNumString(long long aNumb, long aNumLimit, const char* scoreSuffix, const char* aDelimiter)
{
    string finalStr = "";
    if (llabs(aNumb) > aNumLimit) {
        aNumb = aNumb/1000;
        finalStr = string(scoreSuffix);
    }
    return getDelimiteredString(aNumb,3,aDelimiter)+finalStr;
}

bool Util::unzipFileAtPath(const char *aSourcePath,const char *aDestinationPath, bool shouldOverWrite){
    if(!aSourcePath || !aDestinationPath){
        CCLOGERROR("Unzip , aSourcePath or destination is empty");
        return false;
    }
    if(!Util::isFileExistAtPath(aSourcePath)){
        CCLOGERROR("File do not exist at source path %s ", aSourcePath);
        return false;
    }
    if(Util::createDirectoryStructure(aDestinationPath)==false){
        CCLOGERROR("Can not create directory at path %s ", aDestinationPath);
        return false;
    }
	
    bool isSuccess = true;
	
    string unZipPath = aDestinationPath;
    
    unzFile _unzFile = unzOpen( aSourcePath);
    
	if( _unzFile ){
		int ret = unzGoToFirstFile( _unzFile );
        unsigned char buffer[4096] = {0};
        if( ret!=UNZ_OK ){
            return false;
        }
        do{
            ret = unzOpenCurrentFile( _unzFile );
            if( ret!=UNZ_OK ){
                isSuccess = false;
                break;
            }
            int read ;
            unz_file_info	fileInfo = {0};
            ret = unzGetCurrentFileInfo(_unzFile, &fileInfo, NULL, 0, NULL, 0, NULL, 0);
            if( ret!=UNZ_OK ){
                isSuccess = false;
                unzCloseCurrentFile( _unzFile );
                break;
            }
            char* filename = (char*) malloc( fileInfo.size_filename +1 );
            unzGetCurrentFileInfo(_unzFile, &fileInfo, filename, fileInfo.size_filename + 1, NULL, 0, NULL, 0);
            filename[fileInfo.size_filename] = '\0';
            bool isDirectory = false;
            if( filename[fileInfo.size_filename-1]=='/' || filename[fileInfo.size_filename-1]=='\\')
                isDirectory = true;
            string fullPath = unZipPath + filename;
            free( filename );
            if (isDirectory){
                Util::createDirectoryStructure(fullPath.c_str());
            }
            FILE* fp = fopen( fullPath.c_str(), "wb");
            while( fp ){
                read=unzReadCurrentFile(_unzFile, buffer, 4096);
                if( read > 0 ){
                    fwrite(buffer, read, 1, fp );
                }
                else if( read<0 ){
                    break;
                }
                else
                    break;
            }
            if( fp ){
                fclose( fp );
            }
            unzCloseCurrentFile( _unzFile );
            ret = unzGoToNextFile( _unzFile );
        }while( ret==UNZ_OK && UNZ_OK!=UNZ_END_OF_LIST_OF_FILE );
	}
    else{
        isSuccess = false;
    }
    
	return isSuccess;
}



NS_CL_END