/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MGameStateController.h"
#include "MetaHub.h"
#include "CoLibCore.h"
#include "MGameObject.h"
#include "MAppLoader.h"

NS_CL_BEGIN

#define TAG_GAME_SYNC_REQUEST       1


MGameStateController::MGameStateController(MGameObjectsCoreFactory *factoryInstance)
{
    MCR_RETAINED_ASSIGN(_factoryInstance, factoryInstance)
    _isDirty = false;
    _inProgress = false;
    _deltaObjects = new Dictionary();
    _currentObjects = new Dictionary();
    _serviceHandler = NULL;
    _refreshTime = M_GAME_STATE_POST_TIME;
}
MGameStateController::~MGameStateController()
{
    releaseServiceHandler();
    CC_SAFE_RELEASE(_factoryInstance);
    CC_SAFE_RELEASE(_deltaObjects);
    CC_SAFE_RELEASE(_currentObjects);
}
void MGameStateController::addObject(MGameObject *obj)
{
    if(obj)
    {
        MUserStateResponse *userResp =  MUtil::getUserStateResponse();
        obj->setObjectId(userResp->getSeqGlobal());
        userResp->incrementSeqGlobal();
        std::string tableName = obj->getTableName();
        Dictionary *tableDict = (Dictionary*)_deltaObjects->objectForKey(tableName);
        if(!tableDict)
        {
            tableDict = Dictionary::create();
            _deltaObjects->setObject(tableDict, tableName);
        }
        Dictionary *objectData = Dictionary::create();
        MLeaf *metaObj = obj->getMetaObject();
        objectData->setObject(obj->toDict(), M_KEY_DATA);
        objectData->setObject(Double::create(AppLoader::getInstance()->getServerTime()), M_KEY_TIME);
        if(metaObj)
            objectData->setObject(String::create(metaObj->getKey()), KEY_META_ID);
        const char *objId = String::createWithFormat("%u",obj->getObjectId())->getCString();
        tableDict->setObject(objectData, objId);
        
        // add this object in current game state
        tableDict = (Dictionary*)_currentObjects->objectForKey(tableName);
        if(!tableDict)
        {
            tableDict = Dictionary::create();
            _currentObjects->setObject(tableDict, tableName);
        }
        tableDict->setObject(obj, objId);
        _isDirty = true;
    }
}
void MGameStateController::updateObject(MGameObject *obj)
{
    if(obj)
    {
        std::string tableName = obj->getTableName();
        Dictionary *tableDict = (Dictionary*)_deltaObjects->objectForKey(tableName);
        if(!tableDict)
        {
            tableDict = Dictionary::create();
            _deltaObjects->setObject(tableDict, tableName);
        }
        Dictionary *objectData = Dictionary::create();
        MLeaf *metaObj = obj->getMetaObject();
        objectData->setObject(obj->toDict(), M_KEY_DATA);
        objectData->setObject(Double::create(AppLoader::getInstance()->getServerTime()), M_KEY_TIME);
        if(metaObj)
            objectData->setObject(String::create(metaObj->getKey()), KEY_META_ID);
        const char *objId = String::createWithFormat("%u",obj->getObjectId())->getCString();
        tableDict->setObject(objectData, objId);
        _isDirty = true;
        
        // same object ref is updated by player so no need of updating _currentObjects
    }
}
void MGameStateController::deleteObject(MGameObject *obj)
{
    if(obj)
    {
        std::string tableName = obj->getTableName();
        Dictionary *tableDict = (Dictionary*)_deltaObjects->objectForKey(tableName);
        if(!tableDict)
        {
            tableDict = Dictionary::create();
            _deltaObjects->setObject(tableDict, tableName);
        }
        Dictionary *objectData = Dictionary::create();
        MLeaf *metaObj = obj->getMetaObject();
        objectData->setObject(obj->toDict(), M_KEY_DATA);
        objectData->setObject(Double::create(AppLoader::getInstance()->getServerTime()), M_KEY_TIME);
        if(metaObj)
            objectData->setObject(String::create(metaObj->getKey()), KEY_META_ID);
        objectData->setObject(Integer::create(1), M_KEY_DELETE);
        const char * objId = String::createWithFormat("%u",obj->getObjectId())->getCString();
        tableDict->setObject(objectData, objId);
        
        // delete from current game state objects
        tableDict = (Dictionary*)_currentObjects->objectForKey(tableName);
        if(tableDict)
        {
            tableDict->removeObjectForKey(objId);
        }
        _isDirty = true;
    }
}
void MGameStateController::setPlayerGameState(cocos2d::Dictionary *gDict)
{
    CC_SAFE_RELEASE_NULL(_currentObjects);
    _currentObjects = gDict;
    CC_SAFE_RETAIN(_currentObjects);
}
MGameObjectsCoreFactory *MGameStateController::getGameObjectFactory()
{
    return _factoryInstance;
}
void MGameStateController::setRefreshTime(unsigned short t)
{
    SCHEDULAR->unscheduleSelector(schedule_selector(MGameStateController::checkUpdates),this);
    _refreshTime = t;
    SCHEDULAR->scheduleSelector(schedule_selector(MGameStateController::checkUpdates),this,_refreshTime,kRepeatForever,0.0f,false);
}
MGameObject *MGameStateController::getGameObject(TID objId, const char *tableName /*= NULL*/)
{
    MGameObject *obj = NULL;
    if(tableName != NULL)
    {
        Dictionary *tableDict = (Dictionary*)_currentObjects->objectForKey(tableName);
        obj = getObjectFromTable(tableDict,objId);
        if(obj)
            return obj;
    }
    else
    {
        DictElement* pElement = NULL;
        CCDICT_FOREACH(_currentObjects, pElement)
        {
            Dictionary *tableDict = (Dictionary *)pElement->getObject();
            obj = getObjectFromTable(tableDict, objId);
            if(obj)
                return obj;
        }
    }
    return NULL;
}
#pragma mark MServiceHandler delegate methods
void MGameStateController::serviceSuccess(MServiceHandler* aServiceHandler){
    CCLOG("MGameStateController: Service Success for tag %d ", aServiceHandler->getTag());
    
    MUserStateResponse *userResp =  MUtil::getUserStateResponse();
   userResp->updateFromRefresh(aServiceHandler->getResponse()->getDictData());
    double serverUpdateTime = userResp->getMergedAt();
    switch (aServiceHandler->getTag()){
        case TAG_GAME_SYNC_REQUEST:
        {
            if(_deltaObjects && _deltaObjects->count() > 0)
            {
                DictElement* pElement = NULL;
                CCDICT_FOREACH(_deltaObjects, pElement)
                {
                    //const char *tableName = pElement->getStrKey();
                    Dictionary *tableDict = (Dictionary *)pElement->getObject();
                    if(tableDict && tableDict->count() > 0)
                    {
                        DictElement* tableElement = NULL;
                        CCDICT_FOREACH(tableDict, tableElement)
                        {
                            Dictionary *objData = (Dictionary*)tableElement->getObject();
                            double objUpdateTime = ((Double*)objData->objectForKey(M_KEY_TIME))->getValue();
                            if(objUpdateTime < serverUpdateTime)
                            {
                                tableDict->removeObjectForElememt(tableElement);
                            }
                        }
                    }
                }
            }
            _inProgress = false;
            
        }
        break;
        default:
        break;
    }
    
}

void MGameStateController::serviceFailed (MServiceHandler* aServiceHandler,int aStatusCode){
    CCLOG("MGameStateController::Service Failed for tag %d ", aServiceHandler->getTag());
    switch (aServiceHandler->getTag()){
        case TAG_GAME_SYNC_REQUEST:
        {
            _inProgress = false;
        }
            break;
        default:
            break;
    }
}
Dictionary *MGameStateController::getTableObjects(const char *tableName)
{
    return (Dictionary*)_currentObjects->objectForKey(tableName);
}
#pragma mark private methods
void MGameStateController::releaseServiceHandler()
{
    if(_serviceHandler){
        _serviceHandler->invalidate();
        _serviceHandler->release();
    }
}
void MGameStateController::checkUpdates(float dt)
{
    if(!_inProgress && _isDirty && _deltaObjects->count() > 0)
    {
        _inProgress = true;
        _isDirty = false;
        Dictionary *dict = Dictionary::create();
        dict->setObject(_deltaObjects, M_KEY_STATE_DELTA);
        
        dict->setObject(CoLib::AppLoader::getInstance()->getUserStateResponse()->getDictCurrencies(), M_KEY_CURRENCIES);
        dict->setObject(CoLib::AppLoader::getInstance()->getUserStateResponse()->getDictScores(), M_KEY_SCORES);
        dict->setObject(CoLib::AppLoader::getInstance()->getUserStateResponse()->getDictSequences(), M_KEY_SEQUENCES);
        
        releaseServiceHandler();
        _serviceHandler = new MServiceHandler(this, "/refresh", TAG_GAME_SYNC_REQUEST,kHttpRequestTypePost);
        _serviceHandler->start(dict);
    }
}
MGameObject* MGameStateController::getObjectFromTable(Dictionary * dict,TID objId)
{
    if(dict && dict->count() > 0)
    {
        DictElement* pElement = NULL;
        CCDICT_FOREACH(dict, pElement)
        {
            MGameObject *obj = (MGameObject *)pElement->getObject();
             if( objId == obj->getObjectId() )
                 return obj;
        }
    }
    return NULL;
}
NS_CL_END