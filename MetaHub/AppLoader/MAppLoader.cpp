/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MAppLoader.h"
#include "MConfigResponse.h"
#include "MUserStateResponse.h"
#include "MSocialUser.h"
#include "MResponse.h"
#include "CoLibSqlite.h"

NS_CL_BEGIN

#define KEY_PERSIS_CURR_SOCIAL_USER_DATA "__12343sasHJSIsdfer9983"
#define KEY_PERSIS_CURR_SOCIAL_USER_SH   "__88283JJh223GH23fdddff"
#define TIME_FIXABLE_THRESHOLD      180
#define LOCAL_TIME_THRESHOLD        60
#define KEY_SRV_TIME_DIFF           "asdf_srvtdiff"
#define KEY_SRV_TIME                "hhfdd_srvtime"
#define KEY_LOCAL_SAVED_TIME        "kkjjdfloctime"

typedef enum kMServiceTag{
    kMServiceTagConfig = 0,
    kMServiceTagSocialUser ,
    kMServiceTagUserState
}kMServiceTag;

AppLoader* AppLoader::_instance = NULL;

AppLoader::AppLoader(){
    _serviceHandler = NULL;
    _isConfigured   = false;
    
    _filesHandler   = NULL;
    
    _languageType   = LanguageType::ENGLISH;
    _languageAlias = Util::getLanguageAlias(_languageType);
    
    _configObj     = NULL;
    _userStateObj  = NULL;
    _socialUserObj = NULL;
    
    _delegate      = NULL;
    
    _loaderState   = kMAppLoaderStateInitial;   //loader state i.e config, or social or connect
    _serverTimeDiff     = 0.0;
    _useLocalTime       = false;
    _timeConfigured     = false;
    
    _isFailed       = false;
    _delegateSent   = false;
}
AppLoader::~AppLoader(){
    releaseServiceHandler();
}

AppLoader* AppLoader::getInstance(){
    if (!_instance){
        _instance = new AppLoader();
    }

    return _instance;
}

void AppLoader::start(AppLoaderProtocol *aDelegate){
    if(_isFailed || _loaderState == kMAppLoaderStateInitial){
        _delegate = aDelegate;
        
        _isFailed       = false;
        _delegateSent   = false;
        
        switch (_loaderState) {
            case kMAppLoaderStateInitial:
            case kMAppLoaderStateSocialAuto:
                startSocialRequest();
                break;
            case kMAppLoaderStateGetConfig:
                startConfigRequest();
                break;
            case kMAppLoaderStateDownloadFiles:
                startMetaFilesRequest();
                break;
            case kMAppLoaderStateParsing:
                startMetaFilesRequest();
                break;
            case kMAppLoaderStateUserState:
                startUserStateRequest();
                break;
            default:
                CCLOGERROR("invalid case passed in loader");
                assert(true);
        }
        
    }
}

void AppLoader::doneLoading(){
    _loaderState = kMAppLoaderStateAppLoadingDone;
}

MGameStateController* AppLoader::getGameStateController(){
    return _gameStateController;
}

void AppLoader::startSocialRequest(){
    _loaderState = kMAppLoaderStateSocialAuto;
    _delegate->loaderProgress(this, kMAppLoaderStateSocialAuto);
    string jsonStr = MCR_UD_GET_STRING(KEY_PERSIS_CURR_SOCIAL_USER_DATA);
    MCR_RETAINED_ASSIGN(_socialUserObj, MSocialUser::create(jsonStr.c_str(), NULL));

    if(_socialUserObj){ // user already persisted so don't go at server to get it again
        startConfigRequest(); //start the config request as user data is properly filled
    }else{
        releaseServiceHandler();
        _serviceHandler = new MServiceHandler(this, "/myuser", (int)kMServiceTagSocialUser,kHttpRequestTypePost);
        _serviceHandler->start(NULL);
    }
}

void AppLoader::startConfigRequest(){
    _loaderState = kMAppLoaderStateGetConfig;
    _delegate->loaderProgress(this, kMAppLoaderStateGetConfig);
    releaseServiceHandler();
    _serviceHandler = new MServiceHandler(this, "/config", (int)kMServiceTagConfig,kHttpRequestTypeGet);
    _serviceHandler->start(NULL);
}

void AppLoader::startMetaFilesRequest(){
    _loaderState = kMAppLoaderStateDownloadFiles;
    _delegate->loaderProgress(this, kMAppLoaderStateDownloadFiles);
    if(!_filesHandler){
        _filesHandler = new MFilesHandler();
    }
    _filesHandler->start(this);
}

bool AppLoader::startParsing(){
    if(!_delegate) return false;
    bool result = false;
    try{
        _loaderState = kMAppLoaderStateParsing;
        _delegate->loaderProgress(this, kMAppLoaderStateParsing);
        
        MConfigResponse *configObj = MUtil::getConfigResponse();
        Dictionary      *dictFiles = configObj->getDictFilesInfo();
        
        int filesCount = MCR_DICT_COUNT(dictFiles);
        if(filesCount > 0){
            DictElement *element = NULL;
            CCDICT_FOREACH(dictFiles, element){
                string fileId = element->getStrKey();
                string fileRefId = MUtil::getConfigResponse()->getCurrUserFileRefForFileId(fileId.c_str());
                if(fileRefId.empty()){
                    CCLOGWARN("start Parsing:: Skipping [file id = %s] due to empty ref id ",fileId.c_str());
                    continue;
                }
                
                string savedFilePath  = MUtil::getMetaFilePath(fileId);

                if(!CCFileUtils::getInstance()->isFileExist(savedFilePath)){
                    CCLOGERROR("Meta File does not exist at path %s ", savedFilePath.c_str());
                    continue;
                }
                
                SqliteController *sqliteController = SqliteController::create(savedFilePath.c_str(), 1);
                if(!sqliteController){
                    CCLOGERROR("Meta File Couldn't make sqlite object");
                    result = false;
                }else{
                    sqliteController->retain();
                    _delegate->parseFileWithDBRef(sqliteController, fileId.c_str());
                }
            }
            result = true;
        }
    } catch (const CoLib::Exception &except) {
        result = false;
    }
    
    
    return result;
}
void AppLoader::startUserStateRequest(){
    _loaderState = kMAppLoaderStateUserState;
    _delegate->loaderProgress(this, kMAppLoaderStateUserState);
    releaseServiceHandler();
    _serviceHandler = new MServiceHandler(this, "/mystate", (int)kMServiceTagUserState,kHttpRequestTypePost);
    _serviceHandler->start(NULL);
}

#pragma mark getters

string AppLoader::getLanguageAlias(){
    return _languageAlias;
}

LanguageType AppLoader::getLanguageType(){
    return _languageType;
}
kMAppLoaderState AppLoader::getLoaderState(){
    return _loaderState;
}

MSocialUser* AppLoader::getSocialUser(){
    return _socialUserObj;
}

MConfigResponse* AppLoader::getConfigResponse(){
    return _configObj;
}

MUserStateResponse* AppLoader::getUserStateResponse(){
    return _userStateObj;
}

#pragma mark private method implementation

void AppLoader::releaseFilesHandler(){
    
}

void AppLoader::releaseServiceHandler(){
    if(_serviceHandler){
        _serviceHandler->invalidate();
        _serviceHandler->release();
    }
}

void  AppLoader::manageFailed(string aMessage,int aStatusCode){
    if(!_delegate || _delegateSent){
        CCLOGWARN("tried to call manageFailedAgain");
        return;
    }
    _isFailed       = true;
    _delegateSent   = true;
    _delegate->loaderFailed(this, aMessage, aStatusCode);
}

void AppLoader::manageSocialUserResponse(MServiceHandler* aServiceHandler){
    MCR_RETAINED_ASSIGN(_socialUserObj, MSocialUser::create(aServiceHandler->getResponse()->getDictData()));
    if(_socialUserObj){
        //persist the user Obj
        string socialUserJson = _socialUserObj->toJSON();
        MCR_UD_SET_STRING(socialUserJson.c_str(), KEY_PERSIS_CURR_SOCIAL_USER_DATA);
        MCR_UD_PERSIST;
    startConfigRequest();
    } //else manage failed with message social user create error
}

void AppLoader::manageConfigResponse(MServiceHandler* aServiceHandler){
    _configObj->processData(aServiceHandler->getResponse()->getDictData());
    setServerTimeDiff(aServiceHandler->getResponse()->getTime());
    if(_configObj->getDataProcessStatus()){
        startMetaFilesRequest();
    }
    else{
        manageFailed("Unable to process the config response", M_STATUS_CODE_INVALID_CONFIG_RESPONSE);
    }
}
void AppLoader::manageUserStateResponse(MServiceHandler* aServiceHandler){
    _userStateObj->processData(aServiceHandler->getResponse()->getDictData());
    _delegate->loaderSuccess(this);
}

#pragma mark Time calculation
time_t AppLoader::getServerTime()
{
    if(!_timeConfigured)
    {
        CCLOG("ERROR: App is not loaded!!!");
        CCASSERT(false, "Server time is called before the app is loaded");
    }
    if(_useLocalTime)
    {
		return time(NULL);
	}
    else //use server time
    {
		time_t localTime = time(NULL);
		time_t serverTime = localTime + _serverTimeDiff;
		return serverTime;
    }
}

long AppLoader::calculateTimeDifferenceFromServer(time_t serverTime)
{
	time_t t = 0.0;
	if (serverTime != 0.0)
    {
		t = serverTime;
	}
    time_t localTime = time(NULL);
	return (t - localTime);
}

void AppLoader::setServerTimeDiff(time_t aSrvTime)
{
    long savedTime = getPersistedServerTime();
    time_t serverTime = aSrvTime;
    time_t localTime = time(NULL);
    long currentTimeDiff = calculateTimeDifferenceFromServer(serverTime);
    _serverTimeDiff = currentTimeDiff;
    if( getPersistedServerTime() && getPersistedServerTimeDiff() && getPersistedLocalTime() && serverTime <= savedTime)
    {
        long localTimeDiff = localTime - getPersistedLocalTime();
        if(localTimeDiff <= 0 || localTimeDiff > LOCAL_TIME_THRESHOLD )
        {
            persistServerTime(serverTime);
        }
        else
        {
            double savedTimeDiff = getPersistedServerTimeDiff();
            long fixableDiff = 0;
            if(currentTimeDiff < savedTimeDiff)
            {
                fixableDiff = savedTimeDiff - currentTimeDiff;
                if(fixableDiff > TIME_FIXABLE_THRESHOLD)
                {
                    fixableDiff = 0;
                }
            }
            _serverTimeDiff = currentTimeDiff + fixableDiff;
        }
    }
    else
    {
        persistServerTime(serverTime);
    }
    persistLocalTime(localTime);
    persistServerTimeDiff(_serverTimeDiff);
    MCR_USER_DEFAULTS->flush();
    _timeConfigured = true;
}

double AppLoader::getPersistedServerTimeDiff()
{
    double num = MCR_USER_DEFAULTS->getDoubleForKey(KEY_SRV_TIME_DIFF);
    return num;
}

void AppLoader::persistServerTimeDiff(long timeDiff)
{
    MCR_USER_DEFAULTS->setDoubleForKey(KEY_SRV_TIME_DIFF, timeDiff);
}

void AppLoader::persistServerTime(time_t aSrvTime)
{
    MCR_USER_DEFAULTS->setDoubleForKey(KEY_SRV_TIME, aSrvTime);
}

time_t AppLoader::getPersistedServerTime()
{
    double num = MCR_USER_DEFAULTS->getDoubleForKey(KEY_SRV_TIME);
    return num;
}

time_t AppLoader::getPersistedLocalTime()
{
    double num = MCR_USER_DEFAULTS->getDoubleForKey(KEY_LOCAL_SAVED_TIME);
    return num;
}
void AppLoader::persistLocalTime(time_t aSrvTime)
{
     MCR_USER_DEFAULTS->setDoubleForKey(KEY_LOCAL_SAVED_TIME, aSrvTime);
}

#pragma mark Mserver handler methods
void AppLoader::serviceSuccess(MServiceHandler* aServiceHandler){
    CCLOG("AppLoader: Service Success for tag %d ", aServiceHandler->getTag());
    
    switch (aServiceHandler->getTag()){
        case kMServiceTagSocialUser:
            manageSocialUserResponse(aServiceHandler);
            break;
        case kMServiceTagConfig:
            manageConfigResponse(aServiceHandler);
            break;
        case kMServiceTagUserState:
            manageUserStateResponse(aServiceHandler);
            break;
        default:
            manageFailed("AppLoader:: invalid tag for request, un manageble operation for client to handle", M_STATUS_CODE_UNMANAGEABLE_OPERATION);
            break;
    }
    
}

void AppLoader::serviceFailed (MServiceHandler* aServiceHandler,int aStatusCode){
    CCLOG("AppLoader::Service Failed for tag %d ", aServiceHandler->getTag());
    manageFailed(aServiceHandler->getResponse()->getMessage(), aStatusCode);
}

void AppLoader::fileHandlerSuccess(MFilesHandler *aFileHandler){
    CCLOG("AppLoader::fileHandlerSuccess");
    if(startParsing()){
        startUserStateRequest();
    }else{
        manageFailed("Unable to parse the stores", M_STATUS_CODE_META_PARSING_FAILED);
    }
}

void AppLoader::fileHandlerFailed(MFilesHandler *aFileHandler){
    CCLOG("AppLoader::fileHandlerFailed");
    manageFailed("Unable to parse the stores", M_STATUS_CODE_META_FILES_DOWNLOAD_FAILED);
}


NS_CL_END