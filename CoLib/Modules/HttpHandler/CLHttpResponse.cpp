/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLHttpResponse.h"
#include "CLHttpHandler.h"
#include "CLHttpRequest.h"

NS_CL_BEGIN

HttpResponse::HttpResponse(HttpHandler *aHttpHandler,HttpRequest *aRequest)
{
    _networkHandler = aHttpHandler;
    _request        = aRequest;
}

HttpHandler* HttpResponse::getNetworkHandler()
{
    return _networkHandler;
}
HttpRequest* HttpResponse::getRequest()
{
    return _request;
}
kResponseStatusCode HttpResponse::getStatusCode()
{
    return _networkHandler->getStatusCode();
}
char* HttpResponse::getResponseData()
{
    return _networkHandler->getResponseData();
}

string HttpResponse::getMessage()
{
    switch (getStatusCode())
    {
        case kResponseStatusCodeNone:
            return "Unknown or not initialized Response";
            break;
        case kResponseStatusCodeOk:
            return "Response Status is OK";
            break;
        case kResponseStatusCodeFailed:
            return "Unknown error while working on request";
            break;
        case kResponseStatusCodeNoInternet:
            return "No internet connection, please make sure you are connected with internet";
            break;
        case kResponseStatusCodeCanceled:
            return "Request canceled on user's request";
            break;
        case kResponseStatusCodeInvalidRequest:
            return "invalid request";
            break;
        case kResponseStatusCodeTimeout:
            return "Request timeout";
            break;
        case kResponseStatusCodeAuthFailed:
            return "Auth failed by request consumer";
            break;
        case kResponseStatusCodeExceptionInStart:
            return "Unknown exception while starting request";
            break;
        case kResponseStatusCodeExceptionNoData:
            return "Request failed tu to empty data received";
            break;
        case kResponseStatusCodeFileCreateError:
            return "File create error or couldn't write the file";
            break;
        default:
            return "Uknown status code";
            break;
    }
}

#pragma mark Destructor
HttpResponse::~HttpResponse()
{

}

NS_CL_END