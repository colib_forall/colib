/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLMacros_ios_h
#define COLIB_CLMacros_ios_h

#define MCR_IOS_STR(...) [NSString stringWithFormat:__VA_ARGS__]
#define MCR_IOS_DATA(...) [ELNM_STR(__VA_ARGS__) dataUsingEncoding:NSUTF8StringEncoding]
#define MCR_IOS_CHAR_TO_STR(aCharPtr) [NSString stringWithCString:aCharPtr encoding:NSUTF8StringEncoding]
#define MCR_IOS_CSTR_TO_STR(aCStr) [NSString stringWithCString:aCStr.c_str() encoding:NSUTF8StringEncoding]
#define MCR_IOS_CSTR_FROM_STR(aStr) (aStr)?[aStr UTF8String]:[@"" UTF8String]


#endif
