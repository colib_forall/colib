/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MSocialUser.h"

NS_CL_BEGIN

MSocialUser::MSocialUser(){
    _id  = "";
    _tag = "";
    _role= kMUserRoleNormal;
    _dateCreated = 0.0;
    _socialInstanceId = "";
    _localClientToken = "";
}

MSocialUser::~MSocialUser(){
    
}

MSocialUser* MSocialUser::create(cocos2d::Dictionary *aDict){
    if(!aDict) return NULL; // means no user found
    
    string userId = MCR_JDICT_GET_STRING(aDict, M_KEY_SOCIAL_USER_ID, "");
    if(userId.empty()){
        return NULL;
    }
    
    MSocialUser *userObj = new MSocialUser();
    userObj->_id  = userId;
    userObj->_tag = MCR_JDICT_GET_STRING(aDict, M_KEY_SOCIAL_USER_TAG, "");
    userObj->_role = (kMUserRole)MCR_JDICT_GET_INT(aDict, M_KEY_SOCIAL_USER_ROLE, kMUserRoleNormal);
    userObj->_socialInstanceId = MCR_JDICT_GET_STRING(aDict, M_KEY_SOCIAL_INSTANCE_ID, "");
    
#warning replace 0.0 with current time
    userObj->_dateCreated      = MCR_JDICT_GET_DOUBLE(aDict, M_KEY_CREATED_AT, 0.0);
    userObj->_localClientToken = MCR_JDICT_GET_STRING(aDict, M_KEY_CLIENT_TOKEN, "");

    if(userObj->_localClientToken.empty()){
        long long ctime = time(NULL);
        userObj->_localClientToken = userObj->_id + "_" + NUM_TO_STR(ctime) +"_" + Util::numberToString(arc4random()%10000000000);
    }
    
    userObj->autorelease();
    return userObj;
}

MSocialUser* MSocialUser::create(const char *aJsonStr,const char *aHashVal){
    
    //take hash of the user first if has don't match return NULL object
    Dictionary *dictUser = (Dictionary*)JsonManager::convertToObject(aJsonStr);
    return MSocialUser::create(dictUser);
}

string MSocialUser::getId(){
    return _id;
}

string MSocialUser::getTag(){
    return _tag;
}

kMUserRole MSocialUser::getRole(){
    return _role;
}

TTime MSocialUser::getDateCreated(){
    return _dateCreated;
}
string MSocialUser::getSocialInstanceId(){
    return _socialInstanceId;
}

std::string MSocialUser::getLocalClientToken(){
    return _localClientToken;
}

std::string MSocialUser::toJSON(){
    Dictionary *userDict = new Dictionary();
    MCR_DICT_ADD(userDict, String::create(_id), M_KEY_SOCIAL_USER_ID);
    MCR_DICT_ADD(userDict, String::create(_tag), M_KEY_SOCIAL_USER_TAG);
    MCR_DICT_ADD(userDict, Integer::create(_role), M_KEY_SOCIAL_USER_ROLE);
    MCR_DICT_ADD(userDict, Double::create(_dateCreated), M_KEY_CREATED_AT);
    MCR_DICT_ADD(userDict, String::create(_socialInstanceId), M_KEY_SOCIAL_INSTANCE_ID);
    MCR_DICT_ADD(userDict, String::create(_localClientToken), M_KEY_CLIENT_TOKEN);
    string userJson = JsonManager::convertToJson(userDict);
    userDict->release();
    return userJson;
}


NS_CL_END
