/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLMultiDownloader__
#define COLIB_CLMultiDownloader__

#include "CoLibCore.h"
#include "CLHttpHandler.h"

NS_CL_BEGIN

class CLMultiDownloaderDelegate;
class CLMultiDownloader: public cocos2d::Object, public CoLib::CLDownloaderProgressDelegate
{
    
private:
    
    cocos2d::Dictionary           *_downloadHandlers;
    CLMultiDownloaderDelegate     *_mdDelegate;
    kHandlerState                  _multiDownloaderState;
    kResponseStatusCode            _lastFailedStatusCode;
    static pthread_mutex_t  		_sDownloaderMutex;
    static int                      _mutexInitialized;
    int                             _totalProcessedHandlers;
    int                             _totalRequestSent;
    int                             _totalItems;
    int                             _successCount;
    
    CLMultiDownloader(CLMultiDownloaderDelegate *aDelegate, cocos2d::Array *theRequestArray);
    void stepForward();
    void allProcessed();
    
public:
    virtual ~CLMultiDownloader();
    static CLMultiDownloader* create(CLMultiDownloaderDelegate *aDelegate, cocos2d::Array *theRequestArray);
    
    void start();
    void invalidate();
    kResponseStatusCode getLastFailedStatusCode();
    kHandlerState  getHandlerState();
    
    virtual void responseSuccess(HttpResponse *ref);
    virtual void responseFailed(HttpResponse *ref,kResponseStatusCode thestatusCode);
    virtual bool responseAuth(HttpResponse *ref);
    virtual void responseProgress(HttpResponse *ref, long long bytesDone, long long totalBytes,int percentage);
    
};

class CLMultiDownloaderDelegate
{
public:
    virtual void responseSucess(CLMultiDownloader *aRequest) = 0;
    virtual void responseFailure(CLMultiDownloader *aRequest, int failCount)= 0;
    virtual void responseProgress(CLMultiDownloader *aRequest, int aTotalCount,  int successCount) {};
    virtual bool authSubDownload(CLMultiDownloader *aRequest, HttpResponse *aResp) = 0;
    virtual bool authResponse(CLMultiDownloader *ref) = 0;
};

NS_CL_END

#endif
