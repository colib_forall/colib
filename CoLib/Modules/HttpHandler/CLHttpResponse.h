/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLHttpResponse__
#define COLIB_CLHttpResponse__

#include "CoLibCore.h"

NS_CL_BEGIN

USING_NS_CC;
USING_NS_STD;

typedef enum kResponseStatusCode
{
    kResponseStatusCodeNone              = 0,
    kResponseStatusCodeOk                = 1,
    kResponseStatusCodeFailed            = 2,
    kResponseStatusCodeNoInternet        = 3,
    kResponseStatusCodeCanceled          = 4,
    kResponseStatusCodeInvalidRequest    = 5,
    kResponseStatusCodeTimeout           = 6,
    kResponseStatusCodeAuthFailed        = 7,
    kResponseStatusCodeExceptionInStart  = 8,
    kResponseStatusCodeExceptionNoData   = 9,
    kResponseStatusCodeFileCreateError   = 10
}kResponseStatusCode;

class HttpRequest;
class HttpHandler;

class HttpResponse : public Object
{
    
private:
    HttpHandler         *_networkHandler;
    HttpRequest         *_request;
    
public:
    HttpResponse(HttpHandler *aHttpHandler,HttpRequest *aRequest);
    
    HttpHandler*  getNetworkHandler();
    HttpRequest*  getRequest();
    kResponseStatusCode getStatusCode();
    string        getMessage();
    char*         getResponseData();
    
    ~HttpResponse();
};

NS_CL_END

#endif
