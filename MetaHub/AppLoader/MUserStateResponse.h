/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB__MUserStateResponse__
#define COLIB__MUserStateResponse__

#include "CoLibCore.h"

NS_CL_BEGIN

class MUserStateResponse:public Object{

private:
    Dictionary* _dictCurrencies;
    Dictionary* _dictScores;
    Dictionary* _dictSequences;
    
    kDevicePlatform _recentPlatform;
    TTime           _createdAt;
    TTime           _updatedAt;
    TTime           _mergedAt;
    
    bool            _isBanned;
    
    Dictionary* getCurrencyDictForKey(std::string aKey);
    Dictionary* getScoreDictForKey(std::string aKey);
    Dictionary* getSequenceDictForKey(std::string aKey);

public:
    MUserStateResponse();
    ~MUserStateResponse();
    virtual void clear();

    virtual void processData(Dictionary* aDict);
    
    virtual void updateFromRefresh(Dictionary* aDict);
    
    string      getCurrencyValForKey(std::string aKey, std::string aDefVal);
    string      getScoreValForKey(std::string aKey, std::string aDefVal);
    string      getSequenceValForKey(std::string aKey, std::string aDefVal);
    
    void        setScoreValueForKey(string aVal,string aKey);
    void        setCurrencyValueForKey(string aVal,string aKey);
    void        setSequenceValueForKey(string aVal,string aKey);
    
    Dictionary* getDictCurrencies();
    Dictionary* getDictScores();
    Dictionary* getDictSequences();
    
    
    kDevicePlatform     getRecentPlatform();
    TTime               getCreatedAt();
    TTime               getUpdatedAt();
    TTime               getMergedAt();
    bool                isBanned();
    void                incrementSeqGlobal();
    long                getSeqGlobal();
    
};

NS_CL_END

#endif
