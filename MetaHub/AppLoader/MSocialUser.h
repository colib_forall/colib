/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MUserResponse__
#define COLIB_MUserResponse__

#include "CoLibCore.h"
#include "MetaHubCore.h"

NS_CL_BEGIN

class MSocialUser:public Object{

private:
    std::string _id;
    std::string _tag;
    kMUserRole  _role;
    TTime       _dateCreated;
    std::string _socialInstanceId;

    std::string _localClientToken;
    
public:
    
    MSocialUser();
    virtual ~MSocialUser();
    
    static MSocialUser* create(cocos2d::Dictionary *aDict);
    static MSocialUser* create(const char *aJsonStr,const char *aHashVal);
    
    std::string getId();
    std::string getTag();
    kMUserRole  getRole();
    TTime       getDateCreated();
    std::string getSocialInstanceId();
    std::string getLocalClientToken();

    std::string toJSON();
    
};

NS_CL_END

#endif

