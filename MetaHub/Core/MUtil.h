/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MUtil__
#define COLIB_MUtil__

#include <sstream>
#include "MConstants.h"
#include "MTypes.h"
#include "CoLibCore.h"

NS_CL_BEGIN

class AppLoader;
class MSocialUser;
class MConfigResponse;
class MUserStateResponse;
class MGameStateController;

class MUtil{
    
private:
    MUtil(){}
public:
    
    static long long getNumberField(std::stringstream &curRecord);
    static double    getFloatField(std::stringstream &curRecord);
    
    static cocos2d::LanguageType getCurrentLanguageType();
    static std::string           getCurrentLanguageAlias();
    static std::string           appendCurrentLanguageAliasAsSuffix(std::string aStr);
    
    static std::string           getAppId();
    static std::string           getAppSubId();
            
    static std::string           getSerivceURL(std::string aServiceName);
    
    static MSocialUser*          getSocialUser();
    static MConfigResponse*      getConfigResponse();
    static MUserStateResponse*   getUserStateResponse();
    static MGameStateController* getGameStateController();
    
    static cocos2d::Dictionary*  getRequiredParamsDict();
    
    static std::string           getMetaFilesWriteablePath();
    static std::string           getAssetsWriteablePath();
    
    static std::string           getMetaFilePath(string aFileId);
    
    static time_t                getCurrentTime();

    
};


NS_CC_END

#endif
