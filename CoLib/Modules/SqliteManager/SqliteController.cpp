/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "SqliteController.h"
#include "SqliteExceptions.h"

USING_NS_STD;
USING_NS_CL;
USING_NS_CC;
SqliteController::SqliteController(sqlite3 *aSqliteRef){
    sqliteDB_ = aSqliteRef;
}

SqliteController* SqliteController::create(const char *sqliteFilePath, int sqliteOpenMode){
    
    if(!FileUtils::getInstance()->isFileExist(sqliteFilePath)){
        throw ExceptionDbNotFound(SQLITE_NOTFOUND,(string("file does not exist at path:") + sqliteFilePath).c_str());
    }
    
    sqlite3 *sqliteDbRef = NULL;
    int result = sqlite3_open_v2(sqliteFilePath, &sqliteDbRef,sqliteOpenMode,NULL);
    
    if(result != SQLITE_OK)
    {
        throw ExceptionDbOpen(result,(string("file does not exist at path:") + sqliteFilePath).c_str());
    }
    
    SqliteController *sqliteController = new SqliteController(sqliteDbRef);
    sqliteController->_filePath = string(sqliteFilePath);
    sqliteController->autorelease();
    return sqliteController;
}


sqlite3_stmt* SqliteController::executeQuery (const char *aQuery)
{
    sqlite3_stmt *stmt  = NULL;
    const char  *end   = NULL;
    
    if(!aQuery)
    {
        throw ExceptionEmptyQuery(400,"Empty Query passed to execute");
    }
    int result = sqlite3_prepare_v2(sqliteDB_, aQuery, strlen(aQuery)+1, &stmt, &end);
    if(result != SQLITE_OK){
        throw ExceptionStatementMake(result,(string("Unable to make satement with Query: ")+ aQuery + " file path is " + _filePath).c_str()  );
    }
    
    return stmt; // do not forget to call finalize on statment after using it
}

SqliteController::~SqliteController(){
    if(sqliteDB_)
        sqlite3_close_v2(sqliteDB_);
}

