/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MBuy.h"
#include "MetaHubCore.h"

USING_NS_STD
NS_CL_BEGIN


MBuy::MBuy(string aDataRow){
    stringstream record (aDataRow);
    _id      = MUtil::getNumberField(record);
    _price   = MUtil::getNumberField(record);
    _discount= MUtil::getNumberField(record);
    _isDiscountActive= MUtil::getNumberField(record);
}

MBuy::~MBuy(){
    
}

TID MBuy::getID(){
    return _id;
}

TPRICE MBuy::getPrice(){
    return _price;
}

int MBuy::getDiscount(){
    return _discount;
}

bool MBuy::isDiscountActive(){
    return _isDiscountActive;
}

NS_CL_END
