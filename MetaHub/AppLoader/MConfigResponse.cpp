/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MConfigResponse.h"
#include "MetaHubCore.h"
#include "MSocialUser.h"

NS_CL_BEGIN

MConfigResponse::MConfigResponse(){
    _assetURL = "";
    _filesURL = "";
    _liveVersion = 0.0;
    _showUpdateNotification = false;
    _forceUpdate = false;
    _dictFilesInfo = NULL;
    _dataProcessStatus = false;
}

MConfigResponse::~MConfigResponse(){
    clear();
}

void MConfigResponse::clear(){
    CC_SAFE_RELEASE(_dictFilesInfo);
}
void MConfigResponse::processData(Dictionary* aDict){
    clear();
    Dictionary *dictAppConfigs = (Dictionary*)MCR_JDICT_GET_OBJECT(aDict, M_KEY_APP_CONFIGS, NULL);
    Dictionary *dictSubAppConfigs = (Dictionary*)MCR_JDICT_GET_OBJECT(aDict, M_KEY_APP_SUB_CONFIGS, NULL);
    if(dictAppConfigs && dictSubAppConfigs){
    
        _assetURL    = MCR_JDICT_GET_STRING(dictAppConfigs, M_KEY_ASSETS_URL, "");
        _filesURL    = MCR_JDICT_GET_STRING(dictAppConfigs, M_KEY_STORES_URL, "");
        
        _liveVersion = MCR_JDICT_GET_FLOAT(dictSubAppConfigs, M_KEY_LIVE_VERSION, 0.0);
        _showUpdateNotification = MCR_JDICT_GET_INT(dictSubAppConfigs, M_KEY_SHOW_UPDATE_NOTIFICATION, 0.0);
        _forceUpdate   = MCR_JDICT_GET_INT(dictSubAppConfigs, M_KEY_FORCE_UPDATE, 0.0);

        MCR_RETAINED_ASSIGN(_dictFilesInfo, (Dictionary*)MCR_JDICT_GET_OBJECT(aDict, M_KEY_APP_FILES_INFO, NULL)) ;
        _dataProcessStatus = true;
    }else{
        _dataProcessStatus = false;
    }
}

string MConfigResponse::getAssetURL(){
    return _assetURL;
}

string MConfigResponse::getFilesURL(){
    return _filesURL;
}

float  MConfigResponse::getLiveVersion(){
    return _liveVersion;
}

bool MConfigResponse::getShowUpdateNotification(){
    return _showUpdateNotification;
}

bool MConfigResponse::getForceUpdate(){
    return _forceUpdate;
}

Dictionary* MConfigResponse::getDictFilesInfo(){
    return _dictFilesInfo;
}

string MConfigResponse::getCurrUserFileRefForFileId(const char* aFileId){
    Dictionary *dictForFile = (Dictionary*)MCR_JDICT_GET_OBJECT(_dictFilesInfo, aFileId, NULL);
    if (!dictForFile) {
        return "";
    }
    
    if(MUtil::getSocialUser()->getRole() == kMUserRoleTester){
        return MCR_JDICT_GET_STRING(dictForFile, M_KEY_FILE_REF_ID_ADMIN, "");
    }
    return MCR_JDICT_GET_STRING(dictForFile, M_KEY_FILE_REF_ID_CLIENT, "");
}

bool MConfigResponse::getDataProcessStatus(){
    return _dataProcessStatus;
}

NS_CL_END
