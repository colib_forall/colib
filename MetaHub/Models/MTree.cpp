/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MTree.h"
#include "MetaHubCore.h"
#include "MBranch.h"
#include "MContainer.h"


NS_CL_BEGIN

MTree::MTree(){
    _dictBranches = NULL;
}

MTree::~MTree(){
    CC_SAFE_RELEASE(_dictBranches);
}

Dictionary* MTree::getBranches(){
    return _dictBranches;
}

void MTree::initiateParsing(TID aTreeId,SqliteController *aSqliteObj){
    
    string query = "select " +getQueryColumns()+" from trees where _id = "+Util::numberToString(aTreeId) ;
    aSqliteObj->executeQuery<MTree>(query.c_str(), NULL, NULL);
}

/** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
 */
void MTree::processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){
    //call to the parent
    MObject::processData(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    
    _dictBranches = new Dictionary();
    
    M_TREE_DICT->setObject(this, _id);

#ifdef COLIB_META_LOG_SHOW
    CCLOG("Adding Tree %d with name %s ", _id, _name.c_str());
#endif
    
    executeChildQuery(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    
}

/** return the child query.*/
std::string MTree::getChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){
    return "select " +MBranch::getQueryColumns()+" from tree"+Util::numberToString(_id) +"branches";
}

/** virtual method, any class using SqliteController must implement this method , i.e Branch is Child of Tree and Leaf is Child of Branch
 */
void MTree::executeChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult,void* aCustomObj){
    string query = getChildQuery(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    aSqliteControllerObj->executeQuery<MBranch>(query.c_str(), this, aCustomObj);
}
cocos2d::Array* MTree::getSortedBranches()
{
    cocos2d::Array *branchesArray = NULL;
    if(_dictBranches && _dictBranches->count() > 0)
    {
        branchesArray = cocos2d::Array::createWithCapacity(_dictBranches->count());
        DictElement* pElement = NULL;
        CCDICT_FOREACH(_dictBranches, pElement)
        {
            MBranch *aBranch = (MBranch*)pElement->getObject();
            branchesArray->addObject(aBranch);
        }
        //MUtil::sortMObjects(branchesArray);
    }
    return branchesArray;
}

#pragma mark private methods
/**add the branch in _dictBranches for Current Tree reference*/
void MTree::addBranch(MBranch *branch){
    _dictBranches->setObject(branch, branch->getId());
}
NS_CL_END