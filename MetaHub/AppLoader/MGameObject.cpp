/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MGameObject.h"
#include "MLeaf.h"
#include "MContainer.h"

NS_CL_BEGIN

bool MGameObject::init()
{
    _objectId = 0;
    _metaObject = NULL;
    return true;
}
void MGameObject::setMetaObject(std::string aLeafKey)
{
    _metaObject = (MLeaf*)MContainer::getInstance()->getObjectForKey(aLeafKey); // a weak reference of meta object
}
void MGameObject::setMetaObject(MLeaf *objRef)
{
    _metaObject = objRef; // a weak reference of meta object
}
int MGameObject::getObjectType()
{
    return 0;
}
NS_CL_END