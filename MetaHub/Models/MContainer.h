/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef _CoLib__MContainer__
#define _CoLib__MContainer__

#include "CoLibCore.h"

NS_CL_BEGIN

class MObject;
class MTree;
class MBranch;
class MLeaf;
class MCLeaf;

class MContainer{
    friend MObject;
    friend MTree;
    friend MBranch;
    friend MLeaf;
    friend MCLeaf;
    
private:
    static MContainer *_sInstance;
    MContainer();
    cocos2d::Dictionary *_mGlobalDict;
    cocos2d::Dictionary *_mTreeDict;

public:
    static MContainer*     getInstance();
    
    static MObject*     getObjectForKey(std::string aKey);
    static MBranch*     getBranch(TID aTreeId, TID aBranchId);
    static MLeaf*       getLeaf(TID aTreeId, TID aBranchId, TID aLeafId);
    static MCLeaf*      getCLeaf(TID aTreeId, TID aBranchId, TID aLeafId);
    
    void clear();

};

#define M_GLOBAL_DICT MContainer::getInstance()->_mGlobalDict
#define M_TREE_DICT MContainer::getInstance()->_mTreeDict

NS_CL_END


#endif
