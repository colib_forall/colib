/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MServiceHandler.h"
#include "MetaHubCore.h"
#include "MResponse.h"

#define M_HTTP_DEFAULT_TIME_OUT 20

NS_CL_BEGIN


MServiceHandler::MServiceHandler(MServiceHandlerProtocol* aDelegate, string aServiceName, int aTag, kHttpRequestType aServiceMethod){

    _delegate       = aDelegate;
    _tag            = aTag;
    _serviceMethod  = aServiceMethod;
    _serviceName    = aServiceName;
    
    _httpObj        = NULL;
    _responseObj    = NULL;
}

MServiceHandler::~MServiceHandler(){
    invalidate();
    CC_SAFE_RELEASE(_responseObj);
}

void MServiceHandler::initHttpHandler(cocos2d::Dictionary *callSpecificParams){
    
    //build the dictionary for sending request
    
    Dictionary *dictRequestData = new Dictionary();
    
    MCR_DICT_ADD(dictRequestData, MUtil::getRequiredParamsDict(), M_KEY_PARAMS_REQUIRED);
    
    //call specific params
    if(MCR_DICT_COUNT(callSpecificParams)){
        MCR_DICT_ADD(dictRequestData, callSpecificParams, M_KEY_PARAMS_CALL_SPECIFIC);
    }
    
    Dictionary  *paramsDict = Dictionary::create();
    string requestData = JsonManager::convertToJson(dictRequestData);
    
    MCR_DICT_ADD(paramsDict, String::create(requestData.c_str()), "requestData");
    CCLOG("The request data str is %s ", requestData.c_str());
    
    string finalUrl = MUtil::getSerivceURL(_serviceName); //+ "?requestData=" + requestData;
    
    CCLOG("The final MURL is:   %s ", finalUrl.c_str());

    HttpRequest *requestObj = HttpRequest::create(finalUrl, _tag, paramsDict, _serviceMethod);
    requestObj->setTimeoutRequest(M_HTTP_DEFAULT_TIME_OUT);
    requestObj->setTimeoutConnection(M_HTTP_DEFAULT_TIME_OUT);
    //headers here
    //requestObj->seth
    
    _httpObj = HttpHandler::create(this, requestObj);
    _httpObj->retain();
    
}

void MServiceHandler::releaseHttp(){
    if(_httpObj){
        _httpObj->invalidate();
        CC_SAFE_RELEASE_NULL(_httpObj);
    }
}

void MServiceHandler::invalidate(){
    if(!_delegate) return;
    _delegate = NULL;
    releaseHttp();
}

void MServiceHandler::start(cocos2d::Dictionary *callSpecificParams){
    if(_httpObj && _httpObj->getHandlerState() == kHandlerStateInProcess){
        CCLOGWARN("MServiceHandler:: already in progress ServiceName = %s", _serviceName.c_str());
    }
    
    if(!_httpObj){
        //instanitate new object
        initHttpHandler(callSpecificParams);
    }else{
        //release previous and instantiate new
        releaseHttp();
        initHttpHandler(callSpecificParams);
    }
    
    _httpObj->start();
}

int MServiceHandler::getTag(){
    return _tag;
}

MResponse* MServiceHandler::getResponse(){
    return _responseObj;
}

kHandlerState MServiceHandler::getHttpHandlerState(){
    if(_httpObj){
        return _httpObj->getHandlerState();
    }
    return kHandlerStateInitial;
}

#pragma mark protocol methods for HttpHandler

void MServiceHandler::responseSuccess(HttpResponse *responseObj){
    CCLOG("method=success response data  is: %s ", responseObj->getResponseData());
    
    CC_SAFE_RELEASE_NULL(_responseObj);
    _responseObj = new MResponse(responseObj->getResponseData());
    if(_responseObj->getStatus()){
        _delegate->serviceSuccess(this);
    }else{
        _delegate->serviceFailed(this, _responseObj->getCode());
    }
}

void MServiceHandler::responseFailed (HttpResponse *responseObj,kResponseStatusCode theStatusCode){
    CCLOG("method=failed response data is: %s ", responseObj->getResponseData());
    if(!_responseObj || theStatusCode != kResponseStatusCodeOk){
        CC_SAFE_RELEASE_NULL(_responseObj);
        _responseObj = new MResponse(false, theStatusCode, responseObj->getMessage().c_str());
        _delegate->serviceFailed(this, _responseObj->getCode());
    }
}

bool MServiceHandler::responseAuth   (HttpResponse *ref){
    return true;
}


NS_CL_END