/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "SqliteUtil.h"
#include "CoLibCore.h"

NS_CL_BEGIN

string SqliteUtil::getStringVal(sqlite3_stmt *aRowResult, int anIndex,const char *defaultValue)
{
    //const char *strVal  = reinterpret_cast<const char*>(sqlite3_column_text(aRowResult, anIndex));
    const char *strVal  = (char*)sqlite3_column_text(aRowResult, anIndex);
    if(!strVal)
    {
        return defaultValue;
    }
    return string(strVal);
}

int SqliteUtil::getIntVal(sqlite3_stmt *aRowResult, int anIndex)
{
    return sqlite3_column_int(aRowResult, anIndex);
}

double SqliteUtil::getDoubleVal(sqlite3_stmt *aRowResult, int anIndex)
{
    return sqlite3_column_double(aRowResult, anIndex);
}

TBIGVAL SqliteUtil::getInt64(sqlite3_stmt *aRowResult, int anIndex)
{
    return sqlite3_column_int64(aRowResult, anIndex);
}
NS_CL_END