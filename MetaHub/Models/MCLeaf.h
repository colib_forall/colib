/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MCLeave__
#define COLIB_MCLeave__

#include "MLeaf.h"

NS_CL_BEGIN

class MBuy;
class MSell;

class MCLeaf : public MLeaf
{
private:

    static char *_sCleafCols;
    
    int        _visibleFromLevel;
    bool       _isVisible;
    bool       _isNew;
    int        _minVersion;
    int        _maxVersion;
    
    Dictionary *_dictBuyPrices;
    Dictionary *_dictSellPrices;
    Dictionary *_dictUserRoles;
    
public:
    
    MCLeaf();
    virtual ~MCLeaf();
    
    int     getVisibleFromLevel();
    int     getMinVersion();
    int     getMaxVersion();
    bool    isVisible();
    bool    isNew();
    bool     hasAccess(kMUserRole aUserRole);
    
    MBuy*   getBuyObjectForId(int anId);
    MSell*  getSellObjectForId(int anId);
    
    Dictionary* getBuyPrices();
    Dictionary* getSellPrices();
    
    /** returns the Column relavant to current object, concats the parents col via super*/
    static std::string getQueryColumns();
    
    /** returns the colmn count of current MObject*/
    static TID getQueryColumnsCount();
    
    /** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
     */
    virtual void processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj);
};

NS_CL_END


#endif
