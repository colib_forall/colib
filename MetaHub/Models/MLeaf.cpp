/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MLeaf.h"

#include "MetaHubCore.h"
#include "MObject.h"
#include "MBranch.h"

NS_CL_BEGIN

MLeaf::MLeaf(){
    _refBranch = NULL;
}
MLeaf::~MLeaf(){
    _refBranch = NULL;
}

/** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
 */
void MLeaf::processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){
    _refBranch = (MBranch*) aParent;
    //call to the parent
    MObject::processData(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    _refBranch->addLeaf(this);
    
#ifdef COLIB_META_LOG_SHOW
    CCLOG("Adding Leaf %d with name %s ", _id , _name.c_str());
#endif
}

void MLeaf::executeChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult,void* aCustomObj){
    // DO NOTHIN
    // No more childern after leaf, actual leaf data is done here.
}

MBranch* MLeaf::getRefBranch(){
    return _refBranch;
}

string MLeaf::getPath(){
    return _refBranch->getPath()+"/"+NUM_TO_STR(_id);
}

string MLeaf::getKey(){
    return _refBranch->getKey()+"_"+NUM_TO_STR(_id);
}


NS_CL_END
