/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef CoLib_MTypes_h
#define CoLib_MTypes_h

NS_CL_BEGIN

typedef enum kMAssetType{
    kMAssetTypeAny      = 1,
	kMAssetTypeZip      = 2
}kMAssetType;

typedef enum  kMUserRole{
    kMUserRoleNormal     = 0,
    kMUserRoleTester     = 1
}kMUserRole;

typedef enum kMAppLoaderState{
    kMAppLoaderStateInitial,
    kMAppLoaderStateSocialAuto,
    kMAppLoaderStateGetConfig,
    kMAppLoaderStateDownloadFiles,
    kMAppLoaderStateParsing,
    kMAppLoaderStateUserState,
    kMAppLoaderStateDone,
    kMAppLoaderStateAppLoadingDone
}kMAppLoaderState;

typedef enum kLeafType{
    kLeafTypeSimple = 0,
    kLeafTypeComplex = 1
    
}kLeafType;

NS_CL_END

#endif
