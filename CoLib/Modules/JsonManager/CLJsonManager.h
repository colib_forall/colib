/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_JsonManager__
#define COLIB_JsonManager__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CoLibCore.h"

NS_CL_BEGIN
class JsonDict;
class JsonManager
{
    static CSJson::Value dictToJSONValue(cocos2d::Dictionary *dict);
    static CSJson::Value objToJSONValue(cocos2d::Object *obj);
    
    static cocos2d::Array* jsonToCCArray( CSJson::Value &root);
    static cocos2d::Dictionary* jsonToCCDict( CSJson::Value &root);
    static cocos2d::Object* jsonToCCObject( CSJson::Value &root );

public:
    static std::string convertToJson(cocos2d::Object *obj);
    static cocos2d::Object* convertToObject(const char* json);
};
NS_CL_END

#endif
