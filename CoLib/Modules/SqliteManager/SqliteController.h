/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_SqliteController__
#define COLIB_SqliteController__

#include "CoLibCore.h"
#include "lib/sqlite3.h"

/** SqliteController is used to provide fast parsing of models while a single loop of current statement)
 */


NS_CL_BEGIN

class SqliteController: public cocos2d::Object{
    
private:
    string                   _filePath;
    sqlite3                  *sqliteDB_;
    SqliteController(sqlite3 *aSqliteRef);
    sqlite3_stmt* executeQuery (const char *aQuery);
public:
    SqliteController();
    virtual ~SqliteController(void);
    
    /* returns the auto release instance of SqliteController
     */
    static SqliteController* create(const char *sqliteFilePath, int sqliteOpenMode);

    /* Template method to support N kind of Objects implementing SqliteControllerProtocol
     */
    template <class SqliteProtocolClass>
    void  executeQuery(const char *aQuery, void *aParent, void *aCustomObj){
        SqliteProtocolClass *sqliteProtocolObj = NULL;
        
        sqlite3_stmt * stmt= executeQuery(aQuery);
        
        int result = SQLITE_OK;
        try{
            while((result = sqlite3_step(stmt)) == SQLITE_ROW){
                sqliteProtocolObj = new SqliteProtocolClass();
                if(!sqliteProtocolObj){
                    CCLOG("aay kee ho raya aay");
                }
                sqliteProtocolObj->processData(aParent, this, stmt, aCustomObj);
            }
        } catch (CoLib::Exception &except) {
            //try catch just to finalize the the statement then throw
            if(stmt){
                CCLOGERROR("Exception:: %s", except.what());
                sqlite3_finalize(stmt);
                stmt = NULL;
            }
            throw except;
        }
        
        // finally close the statement
        if(stmt){
            sqlite3_finalize(stmt);
        }
        
    }
    
};

NS_CL_END

#endif /* defined(COLIB_SqliteController__) */
