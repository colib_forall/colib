/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MAsset.h"
#include "MetaHubCore.h"

NS_CL_BEGIN
USING_NS_STD

MAsset::MAsset(string aDataRow){
    
    stringstream record (aDataRow);
    _id         = MUtil::getNumberField(record);
    _version    = MUtil::getNumberField(record);
    _isLocal    = MUtil::getNumberField(record);
    _assetType  = (kMAssetType)MUtil::getNumberField(record);
}

MAsset::~MAsset(){
    
}

TID MAsset::getID(){
    return _id;
}

TID MAsset::getVersion(){
    return _version;
}

bool MAsset::isLocal(){
    return _isLocal;
}
kMAssetType MAsset::getAssetType(){
    return _assetType;
}

NS_CL_END
