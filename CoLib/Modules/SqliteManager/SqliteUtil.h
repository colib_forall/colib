/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_SqliteUtil__
#define COLIB_SqliteUtil__

#include "CoLibCore.h"
#include "lib/sqlite3.h"

NS_CL_BEGIN

class SqliteUtil{
private:
    SqliteUtil(){};
public:
    static string          getStringVal(sqlite3_stmt *aRowResult, int anIndex,const char *defaultValue);
    static int             getIntVal(sqlite3_stmt *aRowResult, int anIndex);
    static double          getDoubleVal(sqlite3_stmt *aRowResult, int anIndex);
    static TBIGVAL         getInt64(sqlite3_stmt *aRowResult, int anIndex);
};


NS_CL_END

#endif
