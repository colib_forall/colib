/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MBranch.h"
#include "MetaHubCore.h"
#include "MObject.h"
#include "MLeaf.h"
#include "MCLeaf.h"
#include "MTree.h"
#include "MContainer.h"

NS_CL_BEGIN

char* MBranch::_sBranchQueryCols = NULL;

string MBranch::getQueryColumns(){
    if(!_sBranchQueryCols){
        string *queryCols = new string(MObject::getQueryColumns()+",s_leaf_type");
        _sBranchQueryCols = (char*)queryCols->c_str();
    }
    return _sBranchQueryCols;
}


//string MBranch::getQueryColumns(){
//    string queryCols = MObject::getQueryColumns()+",s_leaf_type";
//    return queryCols;
//}
TID MBranch::getQueryColumnsCount(){
    return MObject::getQueryColumnsCount()+1;
}

MBranch::MBranch(){
    _refTree    = NULL;
    _dictLeaves = NULL;
}
MBranch::~MBranch(){
    _refTree = NULL;
    CC_SAFE_RELEASE(_dictLeaves);
}

/** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
 */
void MBranch::processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){
    _refTree = (MTree*)aParent;
    //call to the parent
    MObject::processData(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    
    _dictLeaves = new Dictionary();
    //work with tree
    _refTree->addBranch(this);

    //work with other properties
    _leafType = (kLeafType)SqliteUtil::getIntVal(aRowResult, MObject::getQueryColumnsCount());
#ifdef COLIB_META_LOG_SHOW
    CCLOG("Adding Branch %d with type %d and name %s ", _id , _leafType, _name.c_str());
#endif
    executeChildQuery(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
}

/** return the child query
 */
std::string MBranch::getChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){
    string query = "select " +MLeaf::getQueryColumns()+" from tree"+NUM_TO_STR(_refTree->getId()) +"branch" + NUM_TO_STR(_id) + "leaves";
    if(_leafType == kLeafTypeComplex){
        query = "select " +MCLeaf::getQueryColumns()+" from tree"+NUM_TO_STR(_refTree->getId()) +"branch" + NUM_TO_STR(_id) + "cleaves";
    }
    
    return query;
}

/** virtual method, any class using SqliteController must implement this method , i.e Branch is Child of Tree and Leaf is Child of Branch
 */
void MBranch::executeChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult,void* aCustomObj){
    
    string query = getChildQuery(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    if(_leafType == kLeafTypeComplex){
        aSqliteControllerObj->executeQuery<MCLeaf>(query.c_str(), this, aCustomObj);
    }else{
        aSqliteControllerObj->executeQuery<MLeaf>(query.c_str(), this, aCustomObj);
    }
}

void MBranch::addLeaf(MLeaf *aLeaf){
    _dictLeaves->setObject(aLeaf, aLeaf->getId());
}

MTree* MBranch::getRefTree(){
    return _refTree;
}

kLeafType MBranch::getLeafType(){
    return _leafType;
}

string MBranch::getPath(){
    return NUM_TO_STR(_refTree->getId())+"/"+NUM_TO_STR(_id);
}

string MBranch::getKey(){
    return _refTree->getKey()+"_"+NUM_TO_STR(_id);
}

Dictionary* MBranch::getLeaves(){
    return _dictLeaves; // return all leaves without any loop
}

Dictionary* MBranch::getLeaves(bool considerVisible,bool considerAppVersion,int aStartLevel, int anEndLevel){
    
    //return all the leaves if the current category type is simple because this don't have nay information about Level, isVisible etc
    if(_leafType == kLeafTypeSimple){
        return getLeaves();
    }
    
    Dictionary *dictLeaves = NULL;
    int leavesCount = MCR_DICT_COUNT(_dictLeaves);
    if(leavesCount > 0){
        dictLeaves = Dictionary::create();

        DictElement *element = NULL;
        CCDICT_FOREACH(_dictLeaves, element){
            int leafId = element->getIntKey();
            MCLeaf* cleaf  = (MCLeaf*)element->getObject();
            bool shouldAddInDict = true;

            if(considerVisible){
                shouldAddInDict = cleaf->isVisible();
            }
            
            if(shouldAddInDict && considerAppVersion){
                shouldAddInDict = (cleaf->getMinVersion() >= Util::getBundledVersion() && cleaf->getMaxVersion() <= Util::getBundledVersion());
            }
            
            if(shouldAddInDict){
                shouldAddInDict = (cleaf->getVisibleFromLevel() >= aStartLevel && cleaf->getVisibleFromLevel() <= anEndLevel);
            }
            
            //now add it in dictionary
            if(shouldAddInDict){
                MCR_DICT_ADD(dictLeaves, cleaf, leafId);
            }
        }
    }
    return dictLeaves;
}
cocos2d::Array *MBranch::getSortedLeafs()
{
    cocos2d::Dictionary *leafs = getLeaves();
    cocos2d::Array *leafArray = NULL;
    if(leafs && leafs->count() > 0)
    {
        leafArray = cocos2d::Array::createWithCapacity(leafs->count());
        DictElement* pElement = NULL;
        CCDICT_FOREACH(leafs, pElement)
        {
            MLeaf *aLeaf = (MLeaf*)pElement->getObject();
            leafArray->addObject(aLeaf);
        }
        //MUtil::sortMObjects(leafArray);
    }
    return leafArray;
}
cocos2d::Array *MBranch::getSortedLeafs(bool considerVisible,bool considerAppVersion,int aStartLevel, int anEndLevel)
{
    cocos2d::Dictionary *leafs = getLeaves();
    cocos2d::Array *leafArray = NULL;
    if(leafs && leafs->count() > 0)
    {
        leafArray = cocos2d::Array::createWithCapacity(leafs->count());
        DictElement* pElement = NULL;
        CCDICT_FOREACH(leafs, pElement)
        {
            MCLeaf *cLeaf = dynamic_cast<MCLeaf*>(pElement->getObject());
            if(cLeaf)
            {
                bool shouldAddInArray = true;
                if(considerVisible)
                {
                    shouldAddInArray = cLeaf->isVisible();
                }
                if(cLeaf && considerAppVersion)
                {
                    shouldAddInArray = (cLeaf->getMinVersion() >= Util::getBundledVersion() && cLeaf->getMaxVersion() <= Util::getBundledVersion());
                }
                if(shouldAddInArray)
                {
                    shouldAddInArray = (cLeaf->getVisibleFromLevel() >= aStartLevel && cLeaf->getVisibleFromLevel() <= anEndLevel);
                }
                if(shouldAddInArray)
                {
                    leafArray->addObject(cLeaf);
                }
            }
        }
        //MUtil::sortMObjects(leafArray);
    }
    return leafArray;
}

NS_CL_END