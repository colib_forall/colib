/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLJsonManager.h"

USING_NS_CC;
USING_NS_CC_EXT;

NS_CL_BEGIN

#pragma mark Private Methods -- Cocos2d Objects ======> JSON
CSJson::Value JsonManager::dictToJSONValue(cocos2d::Dictionary *dict)
{
    CSJson::Value dictValue(CSJson::objectValue);
    cocos2d::Array *keys = dict ? dict->allKeys() : NULL;
    if(keys && keys->count() > 0)
    {
        cocos2d::String *str;
        cocos2d::Object *obj = keys->getObjectAtIndex(0);
        if( (str = dynamic_cast<cocos2d::String *>(obj)) ) // key is a type of 'string' (_dictType = kDictStr)
        {
            for(int i = 0; i < keys->count(); i++)
            {
                std::string key = ((cocos2d::String*)keys->getObjectAtIndex(i))->getCString();
                cocos2d::Object *obj = dict->objectForKey(key);
                dictValue[key] = objToJSONValue(obj);
            }
        }
        else // key is a type of "integer" (_dictType = kDictInt)
        {
            for(int i = 0; i < keys->count(); i++)
            {
                int key = ((cocos2d::Integer*)keys->getObjectAtIndex(i))->getValue();
                cocos2d::Object *obj = dict->objectForKey(key);
                dictValue[key] = objToJSONValue(obj);
            }
        }
    }
    return dictValue;
}

CSJson::Value JsonManager::objToJSONValue( cocos2d::Object *obj )
{
    CSJson::Value v;
    cocos2d::String     *str;
    cocos2d::Integer    *num;
    cocos2d::Bool       *boolean;
    cocos2d::Double     *doubleNum;
    cocos2d::Array      *arr;
    cocos2d::Dictionary *dic;
    
    if( (str = dynamic_cast<cocos2d::String *>(obj)) )
    {
        v = str->getCString();
    }
    else if ((num = dynamic_cast<cocos2d::Integer *>(obj)))
    {
        v = num->getValue();
    }
    else if ((doubleNum = dynamic_cast<cocos2d::Double *>(obj)))
    {
        v = doubleNum->getValue();
    }
    else if ((boolean = dynamic_cast<cocos2d::Bool *>(obj)))
    {
        v = boolean->getValue();
    }
    else if( (arr = dynamic_cast<cocos2d::Array *>(obj)) )
    {
        for(int i = 0; i < arr->count(); i++)
        {
            cocos2d::Object *aObj = arr->getObjectAtIndex(i);
            if(aObj)
            {
                v[i] = objToJSONValue(aObj);
            }
        }
    }
    else if( (dic = dynamic_cast<cocos2d::Dictionary *>(obj)) )
    {
        v = dictToJSONValue(dic);
    }
    return v;
}
#pragma mark Private method -- JSON ======> Cocos2d Objects
cocos2d::Object* JsonManager::jsonToCCObject( CSJson::Value &root )
{
    if(root.isNull())
        return NULL;
        
    if(root.isString())
        return cocos2d::String::create(root.asCString());
    else if(root.isIntegral())
        return cocos2d::String::createWithFormat("%lld",(long long)root.asDouble());
    else if (root.isDouble())
        return cocos2d::String::createWithFormat("%f",root.asDouble());
    else if(root.isBool())
        return cocos2d::String::create( (root.asBool() ? "1" : "0") );
    else if(root.isArray())
        return jsonToCCArray(root);
    else if(root.isObject())
        return jsonToCCDict(root);
    
    return NULL;
}

cocos2d::Array* JsonManager::jsonToCCArray( CSJson::Value &root )
{
    Array *array = Array::create();
    for(int i = 0; i < root.size(); i++)
    {
        Object *obj = jsonToCCObject(root[i]);
        if(obj)
            array->addObject(obj);
    }
    return array;
}

cocos2d::Dictionary* JsonManager::jsonToCCDict( CSJson::Value &root )
{
    cocos2d::Dictionary *data = Dictionary::create();
    std::vector<std::string> keys = root.getMemberNames();
    for(int i = 0; i < keys.size(); i++)
    {
        Object *obj = jsonToCCObject(root[keys[i]]);
        if(obj)
            data->setObject(obj, keys[i]);
    }
    return data;
}
#pragma mark Public methods
std::string JsonManager::convertToJson(cocos2d::Object *obj)
{
    std::string jsonStr = "";
    if(obj)
    {
        CSJson::Value root = objToJSONValue(obj);
        CSJson::FastWriter writer;
        jsonStr = writer.write(root);
    }
    return jsonStr;
}
cocos2d::Object* JsonManager::convertToObject(const char* json)
{
    if(!json)
        return NULL;
    
    CSJson::Reader reader;
    CSJson::Value root;
    
    reader.parse(json, root);
    cocos2d::Object *obj = jsonToCCObject(root);
    return obj;
}


NS_CL_END