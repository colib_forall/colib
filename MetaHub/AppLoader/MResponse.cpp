/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MResponse.h"
#include "MetaHubCore.h"

USING_NS_CC;
USING_NS_STD;

NS_CL_BEGIN

void MResponse::init(){
    _status     = false;
    _code       = M_STATUS_CODE_NONE;
    _message    = "";
    _exception  = "";
    _dictData   = NULL;
}

MResponse::MResponse(const char *aResponseData){
    workWithResponse(aResponseData);
}

MResponse::MResponse(bool aStatus, int aStatusCode,const char *aMessage){
    init();
    _status  = aStatus;
    _code    = aStatusCode;
    _message = aMessage;
}

MResponse::~MResponse(){
    CC_SAFE_RELEASE(_dictData);
}

void MResponse::workWithResponse(const char *aResponsePacket){
    Dictionary *dictResponsePacket = (Dictionary*)JsonManager::convertToObject(aResponsePacket);
    if(!dictResponsePacket){
        _code       = M_STATUS_CODE_INVALID_RESPONSE;
        _message    = _exception = "No or invalid Response from Server";

    }else{
        
        Dictionary *dictStatusData = (Dictionary*)dictResponsePacket->objectForKey(M_KEY_STATUS_DATA);
        Dictionary *dictResponseData = (Dictionary*)dictResponsePacket->objectForKey(M_KEY_RESPONSE_DATA);
        if(!dictStatusData){
            _code    = M_STATUS_CODE_MISSING_STATUS_DATA;
            _message = _exception = "Status Data Packet is missing from Server";
        }
        if(!dictResponseData){
            _code    = M_STATUS_CODE_MISSING_RESPONSE_DATA;
            _message = _exception = "Response Data Packet is missing from Server";
        }
        else{
            _status  = MCR_JDICT_GET_BOOL(dictStatusData,M_KEY_STATUS,false);
            _code    = MCR_JDICT_GET_INT(dictStatusData, M_KEY_CODE, M_STATUS_CODE_NONE);
            _time    = MCR_JDICT_GET_DOUBLE(dictStatusData, M_KEY_TIME, M_STATUS_CODE_NONE);
            _message = MCR_JDICT_GET_STRING(dictStatusData, M_KEY_MESSAGE, "No Message From Server");

            MCR_RETAINED_ASSIGN(_dictData,dictResponseData);
        }
    }
}

bool MResponse::getStatus(){
    return _status;
}

int MResponse::getCode(){
    return _code;
}

TTime MResponse::getTime(){
    return _time;
}

string MResponse::getMessage(){
    return _message;
}

string MResponse::getException(){
    return _exception;
}

string MResponse::getLocalizedMessage(){
    return MCR_LOCALIZED_STR(M_KEY_L_STATUS_STR(_code));
}

Dictionary* MResponse::getDictData(){
    return _dictData;
}



NS_CL_END
