/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLHttpHandler__
#define COLIB_CLHttpHandler__

#include "CoLibCore.h"
#include "curl/curl.h"
#include <fstream>
#include "CLHttpResponse.h"

NS_CL_BEGIN

USING_NS_CC;

class HttpRequest;
class HttpHandlerProtocol;

class HttpHandler : public Object
{
    // variables
    HttpHandlerProtocol         *_delegate;
    HttpRequest                 *_request;
    HttpResponse                *_response;
    
    CURL                        *_curl;
    pthread_t                   _pthread;
    char                        _errorBuffer[CURL_ERROR_SIZE];
    
    // update the below varaibles in callback
    std::vector<char>           _headerData;
    cocos2d::Dictionary        *_headerDict;
    std::vector<char>           _responseData;
    std::ofstream               _fileStream; // if want to load in file
    
    
    kResponseStatusCode       _statusCode;
    kHandlerState             _handlerState;
    bool                      _shouldCancel;
    int                       _tag;
    
    //methods
    HttpHandler(HttpHandlerProtocol *aDelegate, HttpRequest *aRequest); //always use via static method
    ~HttpHandler();
    bool  setCURLCommons(CURL *handle);
    int   treatGetRequest();
    int   treatPostRequest();
    
    void  handleFailed();
    void  handleFailedTask(float dt);
    void  handleResponseSuccess();
    void  handleSuccessTask(float dt);
    
    void prcessResponseData();
    bool createThread(void*);
    
    void preProcessThread();
    void terminateThread();
    void terminateThreadTask();
    
    // static function for thread
    static void* startThread(void* obj);
    static size_t headerHandler(void *ptr, size_t size,
                                size_t nmemb, void *stream);
    
    static size_t responseHandler(void *ptr, size_t size,
                                  size_t nmemb, void *stream);
    
    CURLcode setRequestHeaders(curl_slist *curlHeaders);
    void parseResponseHeader();
    void saveFileData();
    
public:
    
    static HttpHandler* create(HttpHandlerProtocol *aDelegate, HttpRequest *aRequest);
    void   start();
    void   invalidate();
    
    void   updateRequest(HttpRequest* aRequest,HttpHandlerProtocol *aDelegate);
    
    kResponseStatusCode getStatusCode();
    char*  getResponseData(); // data would be NULL if object is of type Downloader
    
    Dictionary  *getHeaderKeys();
    DictElement *getHeaderValueForKey(std::string aKey);
    kHandlerState   getHandlerState();
    void setTag(int tag);
    int getTag();
    cocos2d::Dictionary *getHeaderData();
    
};

class HttpHandlerProtocol
{
public:
    virtual void responseSuccess(HttpResponse *responseObj) = 0;
    virtual void responseFailed(HttpResponse *responseObj,kResponseStatusCode theStatusCode) = 0;
    virtual bool responseAuth(HttpResponse *responseObj) =0;
    
};
class CLDownloaderProgressDelegate : public HttpHandlerProtocol
{
public:
    virtual void responseProgress(HttpResponse *ref, long long bytesDone, long long totalBytes,int percentage) = 0;
};

NS_CL_END


#endif /* defined(COLIB_CLHttpHandler__) */
