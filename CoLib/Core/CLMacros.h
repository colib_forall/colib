/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLMacros_h
#define COLIB_CLMacros_h

#include "CLConstants.h"

NS_CL_BEGIN

//gernal macros

#define DIRECTOR                cocos2d::Director::getInstance()
#define SCHEDULAR               Director::getInstance()->getScheduler()
#define CURR_PLATFORM           cocos2d::CCApplication::getInstance()->getTargetPlatform()

#define NUM_TO_STR(aNum)        Util::numberToString(aNum)

#define MCR_IS_IPHONE_5                (DIRECTOR->getWinSize().width == 1136.0f)
#define MCR_WIN_SIZE                   DIRECTOR->getWinSize()
#define MCR_USER_DEFAULTS               UserDefault::getInstance()

#define MCR_RETAINED_ASSIGN(anObj,aSrc) anObj=aSrc; if(anObj) (anObj)->retain(); else anObj = NULL;

#define MCR_ARR_ADD(anArr,anObj)    {Object* theObjRef=anObj; anArr->addObject(theObjRef);}

//CCDictionary Macros
#define MCR_DICT_ADD(aDict,anObj,aKey)    {Object* theObjRef=anObj; aDict->setObject(theObjRef,aKey);}
#define MCR_DICT_ADD_WR(aDict,anObj,aKey)    {Object* theObjRef=anObj; aDict->setObject(theObjRef,aKey); theObjRef->release();}
#define MCR_DICT_COUNT(aDict)             ((aDict && aDict->count()>0)?aDict->count():0)

//Json DictUtil methods

#define MCR_JDICT_GET_OBJECT(aDict,aKey,aDefVal)     JsonUtil::getObject(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_CCSTRING(aDict,aKey,aDefVal)   JsonUtil::getCCString(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_STRING(aDict,aKey,aDefVal)     JsonUtil::getString(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_BOOL(aDict,aKey,aDefVal)       JsonUtil::getBoolValue(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_INT(aDict,aKey,aDefVal)        JsonUtil::getIntValue(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_FLOAT(aDict,aKey,aDefVal)      JsonUtil::getFloatValue(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_LONG(aDict,aKey,aDefVal)       JsonUtil::getLongValue(aDict,aKey,aDefVal)
#define MCR_JDICT_GET_DOUBLE(aDict,aKey,aDefVal)     JsonUtil::getDoubleValue(aDict,aKey,aDefVal)

#define MCR_JDICT_SET_NUMBER(aDict,aKey,aVal)        JsonUtil::setNumberAsStringValue(aDict,aKey,aVal)

#define MCR_UD_PERSIST                              CCUserDefault::getInstance()->flush()

#define MCR_UD_SET_STRING(aVal,aKey)                CCUserDefault::getInstance()->setStringForKey(aKey, aVal)
#define MCR_UD_SET_INT(aVal,aKey)                   CCUserDefault::getInstance()->setIntegerForKey(aKey, aVal)
#define MCR_UD_SET_FLOAT(aVal,aKey)                 CCUserDefault::getInstance()->setFloatForKey(aKey, aVal)
#define MCR_UD_SET_DOUBLE(aVal,aKey)                CCUserDefault::getInstance()->setDoubleForKey(aKey, aVal)

#define MCR_UD_GET_STRING(aKey)                     CCUserDefault::getInstance()->getStringForKey(aKey)
#define MCR_UD_GET_INT(aKey)                        CCUserDefault::getInstance()->getIntegerForKey(aKey)
#define MCR_UD_GET_FLOAT(aKey)                      CCUserDefault::getInstance()->getFloatForKey(aKey)
#define MCR_UD_GET_DOUBLE(aKey)                     CCUserDefault::getInstance()->getDoubleForKey(aKey)

#define MCR_GAME_STATE_CONTROLLER                   AppLoader::getInstance()->getGameStateController()
#define MCR_GAME_OBJECT_FACTORY                     AppLoader::getInstance()->getGameStateController()->getGameObjectFactory()

NS_CL_END

#endif
