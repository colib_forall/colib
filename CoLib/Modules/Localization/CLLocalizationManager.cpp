/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "CLLocalizationManager.h"
#include "CLJsonManager.h"

USING_NS_CC;
USING_NS_CL;

static LocalizationManager* s_localizationManager = NULL;

LocalizationManager::LocalizationManager(){
    
}

LocalizationManager* LocalizationManager::getInstance() {
    if(s_localizationManager == NULL) {
        s_localizationManager = new LocalizationManager();
        s_localizationManager->init();
    }
    return s_localizationManager;
}

bool LocalizationManager::init() {
    _localizedDict = NULL;
    return true;
}
// load localize file from provided path
void LocalizationManager::configure(const char *aFilePath)
{
    if (aFilePath && FileUtils::getInstance()->isFileExist(aFilePath))
    {
        unsigned long fileSize = 0;
        unsigned char * fileContents = NULL;
        // Get data of file
        fileContents = FileUtils::getInstance()->getFileData( aFilePath , "rb", &fileSize );
        CC_SAFE_RELEASE_NULL(_localizedDict);
        string json = ((const char*)fileContents); // fileContent is a json
        _localizedDict =  (cocos2d::Dictionary*)CoLib::JsonManager::convertToObject(json.c_str());
        CC_SAFE_RETAIN(_localizedDict);
        CC_SAFE_DELETE_ARRAY(fileContents);
    }
    else
    {
        if(aFilePath){
            CCLOGERROR("ERROR! Localized file not found at provided path %s",aFilePath);
        }
        else{
            CCLOGERROR("Empty file path for Localized file");
        }
        CCASSERT(false, "File not found");
    }
}
void LocalizationManager::cleanUp()
{
    CC_SAFE_RELEASE_NULL(_localizedDict);
}
// supporting 9 parameter only 1--9 (single digit)
std::string LocalizationManager::getLocalizedString(const char *key,const char* params[])
{
    if(!_localizedDict)
        return key;
    
    String *str = (String*)_localizedDict->objectForKey(key);
    if(!str)
        return key;
    const char *val = str->getCString();
    
    std::string finalStr;
    if(params)
    {
        int length = strlen(val);
        vector<char> finalVal(length+100);
        for(int i  = 0 ; i < length; i++)
        {
            char aC = val[i];
            if(aC == '%')
            {
                int num = (int)val[i+1] - 48;
                if(num > 0 && num < 10) // checking that a if a number exists after '%' then replace it otherwise normal flow
                {
                    const char *param = params[num-1];
                    for(int j = 0 ; j< strlen(param); j++)
                    {
                        finalVal.push_back(param[j]);
                    }
                    i++; // skipping the number
                }
                else
                {
                    finalVal.push_back(aC); // '%' pushing as it is
                }
            }
            else
            {
                finalVal.push_back(aC);
            }
        }
        finalStr = string(finalVal.begin(),finalVal.end());
    }
    else
    {
        return val;
    }
    return finalStr;
}