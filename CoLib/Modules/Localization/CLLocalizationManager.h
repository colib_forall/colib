/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLLocalizationManager__
#define COLIB_CLLocalizationManager__

#include "CoLibCore.h"

USING_NS_CC;

NS_CL_BEGIN

class LocalizationManager : public Object
{

private:
    cocos2d::Dictionary     *_localizedDict;
    
    LocalizationManager();
    bool init();
    
public:
    static LocalizationManager* getInstance();
    void configure(const char *aFilePath);
    void cleanUp();
    std::string getLocalizedString(const char *key,const char* params[] = NULL);
};

#define MCR_LOCALIZED_STR(aKey)  LocalizationManager::getInstance()->getLocalizedString(aKey)
#define MCR_LOCALIZED_CSTR(aKey) LocalizationManager::getInstance()->getLocalizedString(aKey).c_str()

#define MCR_LOCALIZED_STRP(aKey,anArr)  LocalizationManager::getInstance()->getLocalizedString(aKey,anArr)
#define MCR_LOCALIZED_CSTRP(aKey,anArr) LocalizationManager::getInstance()->getLocalizedString(aKey,anArr).c_str()

NS_CL_END

#endif
