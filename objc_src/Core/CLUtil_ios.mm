/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#import "CLUtil_ios.h"
#include "CLMacros_ios.h"
#include "CLUtil.h"


NS_CL_BEGIN

string Util::stringEscape(const char *aString){
    NSString *urlString = MCR_IOS_CHAR_TO_STR(aString);
    NSString *resultStr = [CLUtil_ios stringEscape:urlString];
    return MCR_IOS_CSTR_FROM_STR(resultStr);
}

bool Util::createDirectoryStructure(const char *aPath)
{
    NSString *path = MCR_IOS_CHAR_TO_STR(aPath);
    return [CLUtil_ios createDirectryStructure:path];
}

bool Util::deleteFileAtPath(const char* aPath){
    NSString *path = MCR_IOS_CHAR_TO_STR(aPath);
    return [CLUtil_ios deleteFileAtPath:path];
}

NS_CL_END


@implementation CLUtil_ios

+(NSString*) stringEscape:(NSString*) aString
{
	return [aString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}

+(BOOL) isDirectoryStructureExists:(NSString *)aDirectoryStructure
{
	BOOL isDirectory =YES;
	if(![aDirectoryStructure hasSuffix:@"/"])
		aDirectoryStructure = [NSString stringWithFormat:@"%@/",aDirectoryStructure];
	return [[NSFileManager defaultManager] fileExistsAtPath:aDirectoryStructure isDirectory:&isDirectory];
}

+ (BOOL) createDirectryStructure:(NSString*) aPath
{
	NSError *err = nil;
	if ([CLUtil_ios isDirectoryStructureExists:aPath]){
		return YES;
	}
	
	if ([[NSFileManager defaultManager] createDirectoryAtPath:aPath withIntermediateDirectories:YES attributes:nil error:&err] || !err){
		return YES;
	}
    
	return NO;
}

+(BOOL) deleteFileAtPath:(NSString*) filePath
{
	// if file already exists, delete it
	NSError *err = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
		if(NO == [[NSFileManager defaultManager] removeItemAtPath:filePath error:&err]){
			return NO;
		}
        
        if(err){
            return NO;
        }
        
	}
	return YES;
}


@end
