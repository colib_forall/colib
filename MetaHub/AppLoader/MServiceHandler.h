/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MServiceHandler__
#define COLIB_MServiceHandler__

#include "CoLibCore.h"
#include "CoLibHttp.h"

NS_CL_BEGIN

class MServiceHandlerProtocol;
class MResponse;

class MServiceHandler:public cocos2d::Object,public HttpHandlerProtocol {

private:
    MServiceHandlerProtocol     *_delegate;
    
    int                         _tag;
    string                      _serviceName;
    kHttpRequestType            _serviceMethod; //GET or POST
    
    HttpHandler                 *_httpObj;
    MResponse                   *_responseObj;
    
    void  initHttpHandler(cocos2d::Dictionary *callSpecificParams);
    void  releaseHttp();
    
public:
    MServiceHandler(MServiceHandlerProtocol* aDelegate, string aServiceName, int aTag, kHttpRequestType aServiceMethod);
    virtual ~MServiceHandler();
    
    void            start(cocos2d::Dictionary *callSpecificParams);
    void            invalidate();
    
    int             getTag();
    MResponse*      getResponse();
    kHandlerState   getHttpHandlerState();
    
    virtual void responseSuccess(HttpResponse *responseObj);
    virtual void responseFailed (HttpResponse *responseObj,kResponseStatusCode theStatusCode);
    virtual bool responseAuth   (HttpResponse *responseObj);
    
};

class MServiceHandlerProtocol{

public:
    virtual void serviceSuccess(MServiceHandler* aServiceHandler) = 0;
    virtual void serviceFailed (MServiceHandler* aServiceHandler,int aStatusCode) = 0;
};

NS_CL_END

#endif
