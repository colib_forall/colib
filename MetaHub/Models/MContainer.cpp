/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MContainer.h"
#include "MObject.h"
#include "MTree.h"
#include "MBranch.h"
#include "MLeaf.h"
#include "MCLeaf.h"

NS_CL_BEGIN

MContainer* MContainer::_sInstance = NULL;

MContainer* MContainer::getInstance(){
    if(!_sInstance){
        _sInstance = new MContainer();
    }
    return _sInstance;
}

MContainer::MContainer(){
    _mGlobalDict = new Dictionary();
    _mTreeDict   = new Dictionary();
}

MObject* MContainer::getObjectForKey(std::string aKey){
    return (MObject*)MContainer::getInstance()->_mGlobalDict->objectForKey(aKey);
}


MBranch* MContainer::getBranch(TID aTreeId, TID aBranchId){
    return (MBranch*)MContainer::getInstance()->_mGlobalDict->objectForKey(NUM_TO_STR(aTreeId)+"_"+NUM_TO_STR(aBranchId));
}

MLeaf* MContainer::getLeaf(TID aTreeId, TID aBranchId, TID aLeafId){
    return (MLeaf*)MContainer::getInstance()->_mGlobalDict->objectForKey(NUM_TO_STR(aTreeId)+"_"+NUM_TO_STR(aBranchId)+"_"+NUM_TO_STR(aLeafId));
}

MCLeaf* MContainer::getCLeaf(TID aTreeId, TID aBranchId, TID aLeafId){
    return (MCLeaf*)MContainer::getLeaf(aTreeId, aBranchId, aLeafId);
}

void MContainer::clear()
{
    //maintain the referece but remove all object
    _mGlobalDict->removeAllObjects();
    _mTreeDict->removeAllObjects();
}

NS_CL_END