/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLUtil__
#define COLIB_CLUtil__

#include "CLConstants.h"
#include "CLTypes.h"

USING_NS_STD;

NS_CL_BEGIN

class Util{
private:
    Util(){}
    
public:

    /** Converts any int to string
     */
    static string  numberToString(int aNumber);
    
    /** Converts any TID to string
     */
    static string  numberToString(TID aNumber);
    
    static string  numberToString(long aNumber);
    
    /** Converts any long number to string
     */
    static string  numberToString(long long aNumber);
    
    /** Converts any float to string
     */
    static string  numberToString(double aDouble);
    
    static string    getLanguageAlias(cocos2d::LanguageType langType);
    
    static string    stringEscape(const char *encodedURL);
    static string    getDelimiteredString(long long aNumb,int delmiterLoc, const char *aDelimiter);
    static string    getShortenedNumString(long long aNumb, long aNumLimit, const char* scoreSuffix, const char* aDelimiter);
    
    static string    getOSVersion();
    static string    getDeviceModel();
    static float     getBundledVersion();
    static float     getBuildNumber();
    static kPlatform getDevicePlatform();
    
    static bool      createDirectoryStructure(const char *aDirectoryPath);
    static bool      isFileExistAtPath(const char* aFilePath);
    
    static bool      unzipFileAtPath(const char *aSourcePath,const char *aDestinationPath, bool shouldOverWrite);
    static bool      deleteFileAtPath(const char* aFilePath);
    
};

NS_CL_END

#endif
