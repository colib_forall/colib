/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MResponse__
#define COLIB_MResponse__

#include "CoLibCore.h"

NS_CL_BEGIN

class MResponse:public cocos2d::Object{

private:
    bool                _status;
    int                 _code;
    TTime               _time;
    string              _message;
    string              _exception;
    
    cocos2d::Dictionary *_dictData;
    
    void init();
    void workWithResponse(const char *aResponseData);
    
public:
    MResponse(const char *aResponseData);
    MResponse(bool aStatus, int aStatusCode,const char *aMessage);
    virtual ~MResponse();
    
    

    bool   getStatus();
    int    getCode();
    TTime  getTime();
    
    string getMessage();
    string getException();
    string getLocalizedMessage();
    
    cocos2d::Dictionary* getDictData();
    
};

#define MCR_RESP_STATUS(aRespObj)              ((aRespObj!=NULL)?aRespObj->getStatus():false)
#define MCR_RESP_STATUS_CODE(aRespObj)         ((aRespObj!=NULL)?aRespObj->getCode():kVG_RESPONSE_CODE_NONE)

#define MCR_RESP_MESSAGE(aRespObj)             ((aRespObj!=NULL)?aRespObj->getMessage():"")
#define MCR_RESP_LOCALIZED_MESSAGE(aRespObj)   (aRespObj) ((aRespObj!=NULL)?aRespObj->getLocalizedMessage():"")
#define MCR_RESP_EXCEPTION(aRespObj)           ((aRespObj!=NULL)?aRespObj->getException():"")


NS_CL_END

#endif
