/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MGameStateController__
#define COLIB_MGameStateController__

#include "cocos2d.h"
#include "CoLibCore.h"
#include "MServiceHandler.h"

NS_CL_BEGIN

class MLeaf;
class MGameObject;
class MGameObjectsCoreFactory;
class MAppLoader;

class MGameStateController : public cocos2d::Object, public MServiceHandlerProtocol
{

    friend MAppLoader;
private:
    bool                    _isDirty;
    bool                    _inProgress;
    MGameObjectsCoreFactory *_factoryInstance;
    cocos2d::Dictionary     *_deltaObjects;
    cocos2d::Dictionary     *_currentObjects;
    MServiceHandler         *_serviceHandler;
    CC_SYNTHESIZE_READONLY(unsigned short, _refreshTime, RefreshTime);
    
    void releaseServiceHandler();
    void checkUpdates(float dt);
    MGameObject* getObjectFromTable(Dictionary * dict,TID objId);
    
public:
    MGameStateController(MGameObjectsCoreFactory *anInstance);
    ~MGameStateController();
    void addObject(MGameObject *obj);
    void updateObject(MGameObject *obj);
    void deleteObject(MGameObject *obj);
    Dictionary *getTableObjects(const char *tableName);
    void setPlayerGameState(cocos2d::Dictionary *gDict);
    MGameObjectsCoreFactory *getGameObjectFactory();
    void setRefreshTime(unsigned short t);
    MGameObject *getGameObject(TID objId, const char *tableName = NULL);
    
    virtual void serviceSuccess(MServiceHandler* aServiceHandler);
    virtual void serviceFailed (MServiceHandler* aServiceHandler,int aStatusCode);
};

class MGameObjectsCoreFactory : public cocos2d::Object
{
public:
    virtual MGameObject* getGameObject(MLeaf *aLeafObj) = 0;
    virtual MGameObject* getGameObject(const char *leafId) = 0;
};

NS_CL_END

#endif
