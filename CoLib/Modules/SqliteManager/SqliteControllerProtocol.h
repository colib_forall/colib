/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ****************************************************************************/


/** SqliteControllerProtocol is a controller protocol class , each object using SqliteController for parsing must inherits it )
 */


#ifndef COLIB_SqliteControllerProtocol_h
#define COLIB_SqliteControllerProtocol_h

#include "CoLibCore.h"
#include "lib/sqlite3.h"

NS_CL_BEGIN

class SqliteController;
class SqliteControllerProtocol{
public:
   
    
    /** virtual method, any class using SqliteController must implement this method , i.e Branch is Child of Tree and Leaf is Child of Branch
     */
    virtual void executeChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult,void* aCustomObj) = 0;
    
    /** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
     */
    virtual void processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj) = 0;
    
    /** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
     */
    virtual std::string getChildQuery(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj) = 0;
    
};

NS_CL_END

#endif
