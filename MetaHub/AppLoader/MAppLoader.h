/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_AppLoader__
#define COLIB_AppLoader__

#include "CoLibCore.h"
#include "MetaHubCore.h"
#include "MServiceHandler.h"
#include "MFilesHandler.h"
#include "MGameStateController.h"

USING_NS_CC;
USING_NS_STD;

NS_CL_BEGIN

class AppLoaderProtocol;
class MConfigResponse;
class MUserStateResponse;
class MSocialUser;
class MFilesHandler;
class SqliteController;

class AppLoader:public cocos2d::Object, MServiceHandlerProtocol, MFilesHandlerProtocol{
    
private:
    
    bool                _isConfigured;  //flag to get if the loader is config

    string              _languageAlias; //alias of langugae the App Loader is configured with i.e "en" for ENGLISH
    LanguageType        _languageType;  //current language type

    MConfigResponse     *_configObj;
    MUserStateResponse  *_userStateObj;
    MSocialUser         *_socialUserObj;
    bool                _timeConfigured;
    time_t              _serverTimeDiff;
    bool                _useLocalTime;
    
    AppLoaderProtocol   *_delegate;
    bool                 _delegateSent;
    bool                 _isFailed;
    
    MServiceHandler     *_serviceHandler;
    kMAppLoaderState     _loaderState;   //loader state i.e config, or social or connect

    MFilesHandler       *_filesHandler;
    
    MGameStateController *_gameStateController;
    

    static AppLoader *_instance;
    AppLoader();
    ~AppLoader(void);
    
    void  releaseServiceHandler();
    void  releaseFilesHandler();
    
    void  manageFailed(string aMessage,int aStatusCode);
    
    void startSocialRequest();
    void startConfigRequest();
    void startMetaFilesRequest();
    void startUserStateRequest();
    bool startParsing();
    
    void manageSocialUserResponse(MServiceHandler* aServiceHandler);
    void manageConfigResponse(MServiceHandler* aServiceHandler);
    void manageUserStateResponse(MServiceHandler* aServiceHandler);
    
    long calculateTimeDifferenceFromServer(time_t serverTime);
    void setServerTimeDiff(time_t aSrvTime);
    double getPersistedServerTimeDiff();
    void persistServerTimeDiff(long timeDiff);
    void persistServerTime(time_t aSrvTime);
    time_t getPersistedServerTime();
    time_t getPersistedLocalTime();
    void persistLocalTime(time_t localTime);

    
public:
    static AppLoader* getInstance();
    void   start(AppLoaderProtocol *aDelegate);
    void   doneLoading();
    MGameStateController*   getGameStateController();
    
    string              getLanguageAlias();
    LanguageType        getLanguageType();
    kMAppLoaderState    getLoaderState();
    
    MSocialUser*        getSocialUser();
    MConfigResponse*    getConfigResponse();
    MUserStateResponse* getUserStateResponse();
    
    time_t              getServerTime();
    
    template<class DerivedConfigClass,class DerivedUserStateClass>
    void configure(cocos2d::LanguageType aLangType, Dictionary *treesToConsider, MGameObjectsCoreFactory *coreFactoryInstance)
    {
        if(_isConfigured)
        {
            CCLOGWARN("App is already Configured");
            return;
        }
        
        _languageType = aLangType;
        _languageAlias = Util::getLanguageAlias(aLangType);
        
        _configObj      = new DerivedConfigClass();
        _userStateObj   = new DerivedUserStateClass();
        _gameStateController = new MGameStateController(coreFactoryInstance);

        _isConfigured = true;
    }
    
    void serviceSuccess(MServiceHandler* aServiceHandler);
    void serviceFailed(MServiceHandler* aServiceHandler,int aStatusCode);
    
    void fileHandlerSuccess(MFilesHandler *aFileHandler);
    void fileHandlerFailed(MFilesHandler *aFileHandler);
};

class AppLoaderProtocol{

public:
    
    virtual void loaderSuccess(AppLoader* aLoaderObj) = 0;
    virtual void loaderFailed(AppLoader* aLoaderObj,string aFailMessage,int statusCode)  = 0;
    virtual void loaderProgress(AppLoader* aLoaderObj, kMAppLoaderState aLoaderState)  = 0;
    virtual void parseFileWithDBRef(SqliteController* sqliteObj, const char* aFileId) = 0;

};

NS_CL_END

#endif
