/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MFilesHandler.h"
#include "MConfigResponse.h"
#include "MetaHubCore.h"
#include "MSocialUser.h"

NS_CL_BEGIN


MFilesHandler::MFilesHandler(){
    _delegate = NULL;
    _multiDownloader = NULL;
}

MFilesHandler::~MFilesHandler(){
    invalidate();
}

void MFilesHandler::start(MFilesHandlerProtocol *aDelegate){
    _delegate = aDelegate;
    if(_multiDownloader && _multiDownloader->getHandlerState() == kHandlerStateInProcess){
        CCLOGWARN("MFilesHandler:: already in progress ServiceName ");
    }
    initMultiDownloader();
}

void MFilesHandler::invalidate(){
    if(!_delegate) return;
    
    releaseMultiDownloader();
    _delegate = NULL;
    
}

void MFilesHandler::initMultiDownloader(){
    releaseMultiDownloader();
    MConfigResponse *configObj = MUtil::getConfigResponse();
    Dictionary      *dictFiles = configObj->getDictFilesInfo();
    Array           *arrRequests = Array::create();
    arrRequests->retain();
    int filesCount = MCR_DICT_COUNT(dictFiles);
    if(filesCount){
        DictElement *element = NULL;
        CCDICT_FOREACH(dictFiles, element){
            string fileId = element->getStrKey();
            string fileRefId = MUtil::getConfigResponse()->getCurrUserFileRefForFileId(fileId.c_str());
            
            if(fileRefId.empty()){
                CCLOGWARN("MFilesHandler:: Skipping [file id = %s] due to empty ref id ",fileId.c_str());
                continue;
            }

            string savedFileRefId = MCR_UD_GET_STRING(getUDKeyForFileId(fileId.c_str()).c_str());
            string savedFilePath  = MUtil::getMetaFilePath(fileId);
            //if saved file key is not equal to new one, then download the file again
            if(!(strcmp(fileRefId.c_str(), savedFileRefId.c_str())==0) || !CCFileUtils::getInstance()->isFileExist(savedFilePath)){
                
                if(!fileRefId.empty()){
                    string directoryToSave = MUtil::getMetaFilesWriteablePath()+fileId;
                    string downloadPath = MUtil::getConfigResponse()->getFilesURL()+"/" +fileId + "/" + fileRefId;
                    
                    #ifdef COLIB_META_LOG_SHOW
                        CCLOG("Meta file download path is %s ", downloadPath.c_str());
                    #endif

                    HttpRequest *requestDownload = HttpRequest::create(downloadPath, std::atoi(fileId.c_str()), NULL, directoryToSave, fileRefId);
                    MCR_ARR_ADD(arrRequests,requestDownload);
                }
            }
        }

        if(filesCount > 0 && arrRequests->count() == 0){
            CC_SAFE_RELEASE_NULL(arrRequests);
            _delegate->fileHandlerSuccess(this);
        }
        
        _multiDownloader = CLMultiDownloader::create(this, arrRequests);
        if(_multiDownloader){
            _multiDownloader->retain();
            _multiDownloader->start();
        }
        CC_SAFE_RELEASE(arrRequests);
    }
}

void MFilesHandler::releaseMultiDownloader(){
    if(_multiDownloader){
        _multiDownloader->invalidate();
        _multiDownloader->release();
    }
}

string MFilesHandler::getUDKeyForFileId(const char* aFileId){
    string key = string("adfefeffeidsafl22d__")+string(aFileId);
    CCLOG("Look for key %s ",key.c_str());
    return key;
}

#pragma mark multidownloader protocol methods

void MFilesHandler::responseSucess(CLMultiDownloader *aRequest){
    CCLOG("MFilesHandler::responseSucess");
    _delegate->fileHandlerSuccess(this);
}
void MFilesHandler::responseFailure(CLMultiDownloader *aRequest, int failCount){
    CCLOG("MFilesHandler::responseFailure");
    _delegate->fileHandlerFailed(this);
}

bool MFilesHandler::authSubDownload(CLMultiDownloader *aRequest, HttpResponse *aResp){
    bool status = true;
    HttpRequest *requestObj = (HttpRequest*) aResp->getRequest();
    if(requestObj){
        string fileId = NUM_TO_STR(requestObj->getTag());
        bool uZipStatus = Util::unzipFileAtPath(requestObj->getFullPath().c_str(), (MUtil::getMetaFilesWriteablePath()+fileId+"/").c_str(), true);
        if(uZipStatus == true){
            string serverFileRefId = MUtil::getConfigResponse()->getCurrUserFileRefForFileId(fileId.c_str());
            MCR_UD_SET_STRING(serverFileRefId,getUDKeyForFileId(fileId.c_str()).c_str());
            MCR_UD_PERSIST;
            status =  true;
        }else{
            status = false;
        }
        Util::deleteFileAtPath(requestObj->getFullPath().c_str());
    }
    CCLOG("MFilesHandler::authSubDownload:: status = %d and tag = %d", status,requestObj->getTag());
    return status;
}

bool MFilesHandler::authResponse(CLMultiDownloader *ref){
    return true;
}

NS_CL_END