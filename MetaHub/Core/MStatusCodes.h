/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_MStatusCodes_h
#define COLIB_MStatusCodes_h

#define M_STATUS_CODE_NONE                      -1

// some generic meta codes.
#define M_STATUS_CODE_INVALID_RESPONSE            50
#define M_STATUS_CODE_MISSING_STATUS_DATA         51
#define M_STATUS_CODE_MISSING_RESPONSE_DATA       52
#define M_STATUS_CODE_UNMANAGEABLE_OPERATION      53
#define M_STATUS_CODE_INVALID_CONFIG_RESPONSE     55
#define M_STATUS_CODE_META_FILES_DOWNLOAD_FAILED  56
#define M_STATUS_CODE_META_PARSING_FAILED         57

// server codes
#define M_STATUS_CODE_OK                          2000


#endif
