/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef CoLib_MConstants_h
#define CoLib_MConstants_h

#include "CoLibCore.h"

NS_CL_BEGIN

#define M_KEY_DATA_RECORD_SEPERATOR_    '|'
#define M_KEY_DATA_FIELD_SEPERATOR      ','

#define M_KEY_L_STATUS_STR(aCode)    std::string("M_KEY_L_STATUS_CODE_"+Util::numberToString(aCode)).c_str()

#define M_KEY_HASH                    "thhsh"
#define M_KEY_REQUEST_DATA            "requestData"
#define M_KEY_RESPONSE_DATA           "responseData"
#define M_KEY_STATUS_DATA             "statusData"

#define M_KEY_STATUS                  "status"
#define M_KEY_CODE                    "code"
#define M_KEY_MESSAGE                 "message"
#define M_KEY_EXCEPTION               "exception"

#define M_KEY_PARAMS_REQUIRED         "paramsRequired"
#define M_KEY_PARAMS_APP_REQUIRED     "paramsAppRequired"
#define M_KEY_PARAMS_CALL_SPECIFIC    "paramsCallSpecific"

#define M_KEY_CLIENT_TOKEN            "clientToken"
#define M_KEY_CLIENT_TIME             "clientTime"
#define M_KEY_APP_ID                  "appId"
#define M_KEY_APP_SUB_ID              "appSubId"
#define M_KEY_CLIENT_VERSION          "clientVersion"
#define M_KEY_PLATFORM                "platform"
#define M_KEY_OS_VERSION              "osVersion"
#define M_KEY_DEVICE_MODEL            "deviceModel"

#define M_KEY_SOCIAL_USER_ID          "uId"
#define M_KEY_SOCIAL_USER_ROLE        "uRole"
#define M_KEY_SOCIAL_USER_TAG         "uTag"
#define M_KEY_SOCIAL_INSTANCE_ID      "socialInstanceId"

#define M_KEY_LIVE_VERSION              "liveVersion"
#define M_KEY_FORCE_UPDATE              "forceUpdate"
#define M_KEY_SHOW_UPDATE_NOTIFICATION  "showUpdateNotification"

#define M_KEY_ASSETS_URL                "assetsURL"
#define M_KEY_STORES_URL                "filesURL"


#define M_KEY_CURRENCIES              "currencies"
#define M_KEY_SCORES                  "scores"
#define M_KEY_SEQUENCES               "sequences"

#define M_KEY_PUSH_TOKEN_IOS          "pushTokenIOS"
#define M_KEY_PUSH_TOKEN_ANDROID      "pushTokenAndroid"
#define M_KEY_CREATED_AT              "createdAt"
#define M_KEY_UPDATED_AT              "updatedAt"
#define M_KEY_MERGED_AT               "mergedAt"
#define M_KEY_IS_BANNED               "isBanned"
#define M_KEY_GAME_STATE              "gameState"

#define M_KEY_STATE_DELTA             "stateDelta"
#define M_KEY_DELETE                  "dl"
#define M_KEY_DATA                    "d"
#define M_KEY_TIME                    "t"
#define M_KEY_VALUE                   "v"
#define KEY_OBJ_ID                    "o"
#define KEY_META_ID                   "i"

#define M_KEY_APP_CONFIGS             "appConfigs"
#define M_KEY_APP_SUB_CONFIGS         "appSubConfigs"
#define M_KEY_APP_FILES_INFO          "appFilesInfo"

#define M_KEY_FILE_ID                 "fileId"
#define M_KEY_FILE_REF_ID_ADMIN       "fileRefIdAdmin"
#define M_KEY_FILE_REF_ID_CLIENT      "fileRefIdClient"
#define M_KEY_FILES_INFO              "filesInfo"

#define M_KEY_APPS                    "apps"
#define M_KEY_FBID                    "fbid"
#define M_KEY_GAMECENTER              "gcid"

#define M_GAME_STATE_POST_TIME         10.0f

NS_CL_END

#endif
