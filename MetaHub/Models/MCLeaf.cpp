/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MCLeaf.h"
#include "MBuy.h"
#include "MSell.h"

NS_CL_BEGIN

char* MCLeaf::_sCleafCols = NULL;


string MCLeaf::getQueryColumns(){
    if(!_sCleafCols){
        string *queryCols = new string(MLeaf::getQueryColumns()+",s_visible_from_level,s_is_visible,s_is_new,s_min_version,s_max_version,s_buy_prices,s_sell_prices,s_user_roles");
        _sCleafCols = (char*)queryCols->c_str();
    }
    return _sCleafCols;
}
TID MCLeaf::getQueryColumnsCount(){
    return MLeaf::getQueryColumnsCount()+8;
}

MCLeaf::MCLeaf(){
    
    _visibleFromLevel = 0;
    _isVisible        = false;
    _isNew            = false;
    _minVersion       = 0;
    _maxVersion       = 0;
    
    _dictBuyPrices  = NULL;
    _dictSellPrices = NULL;
    _dictUserRoles  = NULL;

}
MCLeaf::~MCLeaf(){
    CC_SAFE_RELEASE(_dictBuyPrices);
    CC_SAFE_RELEASE(_dictSellPrices);
}

/** When the execute method of SqliteController is called this virtual method will be behave as callback, here you can process/parse the data or execute more queries if required.
 */
void MCLeaf::processData(void *aParent, SqliteController *aSqliteControllerObj, sqlite3_stmt *aRowResult, void* aCustomObj){
    //call to the parent
    MLeaf::processData(aParent, aSqliteControllerObj, aRowResult, aCustomObj);
    
    //start parsing from index
    int colIndex = MLeaf::getQueryColumnsCount();
    _visibleFromLevel = SqliteUtil::getIntVal(aRowResult, colIndex++);
    _isVisible        = SqliteUtil::getIntVal(aRowResult, colIndex++);
    _isNew            = SqliteUtil::getIntVal(aRowResult, colIndex++);
    _minVersion       = SqliteUtil::getIntVal(aRowResult, colIndex++);
    _maxVersion       = SqliteUtil::getIntVal(aRowResult, colIndex++);
    
    //Buy Prices parsing
    string strCurrentField = SqliteUtil::getStringVal(aRowResult, colIndex++,"");
    if(not strCurrentField.empty()){
        string currRecord;
        stringstream resourceInfoCommaStream(strCurrentField);
        _dictBuyPrices = new Dictionary();
        while( getline(resourceInfoCommaStream, currRecord, M_KEY_DATA_RECORD_SEPERATOR_) ){
            MBuy *buyObj = new MBuy(currRecord);
            MCR_DICT_ADD_WR(_dictBuyPrices, buyObj, buyObj->getID());
        }
    }
    
    //Sell prices parsing
    strCurrentField = SqliteUtil::getStringVal(aRowResult, colIndex++,"");
    if(not strCurrentField.empty()){
        string currRecord;
        stringstream resourceInfoCommaStream(strCurrentField);
        _dictSellPrices = new Dictionary();
        while( getline(resourceInfoCommaStream, currRecord, M_KEY_DATA_RECORD_SEPERATOR_) ){
            MSell *sellObj = new MSell(currRecord);
            MCR_DICT_ADD_WR(_dictSellPrices, sellObj, sellObj->getID());
        }
    }
    
    //User Roles parsing
    strCurrentField = SqliteUtil::getStringVal(aRowResult, colIndex++,"");
    if(not strCurrentField.empty()){
        string currRecord;
        stringstream resourceInfoCommaStream(strCurrentField);
        _dictUserRoles = new Dictionary();
        int userRole = 0;
        while( getline(resourceInfoCommaStream, currRecord, M_KEY_DATA_RECORD_SEPERATOR_) ){
            userRole = atoi(currRecord.data());
            MCR_DICT_ADD_WR(_dictUserRoles,String::create(Util::numberToString(userRole)), userRole);
        }
    }
    
}

int MCLeaf::getVisibleFromLevel(){
    return _visibleFromLevel;
}

int MCLeaf::getMinVersion(){
    return _minVersion;
}

int MCLeaf::getMaxVersion(){
    return _maxVersion;
}

bool MCLeaf::isVisible(){
    return _isVisible;
}

bool MCLeaf::isNew(){
    return _isNew;
}

bool MCLeaf::hasAccess(kMUserRole aUserRole){

    bool hasAccess = false;
    // if user is tester then return true also if no value entered then it means that everyone has access to this object
    if(!_dictUserRoles || aUserRole == kMUserRoleTester){
        hasAccess = true;
    }

    const String *accessObj = _dictUserRoles->valueForKey(aUserRole);
    if(not accessObj){ // i.e open for everyone
        hasAccess = false;
    }
    
    return hasAccess;
}

MBuy* MCLeaf::getBuyObjectForId(int anId){
    
    if(_dictBuyPrices)
    {
        MBuy *buyObj = (MBuy*) _dictBuyPrices->objectForKey(anId);
        if(buyObj)
        {
            return buyObj;
        }
    }
    return NULL;
}

MSell* MCLeaf::getSellObjectForId(int anId){
    if(_dictSellPrices)
    {
        MSell *sellObj = (MSell*) _dictBuyPrices->objectForKey(anId);
        if(sellObj)
        {
            return sellObj;
        }
    }
    return NULL;
}

Dictionary* MCLeaf::getBuyPrices(){
    return _dictBuyPrices;
}

Dictionary* MCLeaf::getSellPrices(){
    return _dictSellPrices;
}


NS_CL_END
