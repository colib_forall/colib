/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLJsonUtil__
#define COLIB_CLJsonUtil__

#include "CoLibCore.h"

NS_CL_BEGIN

class JsonUtil{
    
private:
    JsonUtil();

public:
    
    //cocos objects
    static cocos2d::Object* getObject(cocos2d::Dictionary  *aDict, const char*aKey, cocos2d::Object*  aDefObj);
    static cocos2d::String* getCCString(cocos2d::Dictionary  *aDict, const char*aKey, cocos2d::String*  aDefStr);
    
    //primitivies
    static std::string getString(cocos2d::Dictionary     *aDict, const char*aKey, string aDefVal);
    static bool        getBoolValue(cocos2d::Dictionary  *aDict, const char*aKey, bool  aDefVal);
    static int         getIntValue(cocos2d::Dictionary   *aDict, const char*aKey, int   aDefVal);
    static float       getFloatValue(cocos2d::Dictionary *aDict, const char*aKey, float aDefVal);
    
    static long long   getLongValue(cocos2d::Dictionary   *aDict,const char*aKey, long long   aDefVal);
    static double      getDoubleValue(cocos2d::Dictionary *aDict,const char*aKey, long double aDefVal);
    
    static void        setNumberAsStringValue(cocos2d::Dictionary  *aDict, const char*aKey,long aNumber);
    static void        setNumberAsStringValue(cocos2d::Dictionary  *aDict, const char*aKey,double aNumber);
};

NS_CL_END

#endif
