/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef COLIB_CLEnums_h
#define COLIB_CLEnums_h

#include "CLConstants.h"

NS_CL_BEGIN

typedef unsigned int TID;
typedef long         TPRICE;
typedef long long    TBIGVAL;
typedef long double  TTime;

typedef enum kDevicePlatform{
    kDevicePlatformUnknown = 0,
    kDevicePlatformIOS     = 1,
    kDevicePlatformAndroid = 2
    
} kPlatform;

typedef enum kHandlerState
{
    kHandlerStateInitial      = 0,
    kHandlerStateInProcess    = 1,
    kHandlerStateFailed       = 2,
    kHandlerStateCompleted    = 3,
}kHandlerState;

NS_CL_END


#endif
