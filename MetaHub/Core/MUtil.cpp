/****************************************************************************
 <CoLib an extension for Cocos2dx  to make assets based apps/games>
 Copyright (C) <2012>  <colib: colibforall@gmail.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "MUtil.h"
#include "CoLibCore.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "../AppLoader/MAppLoader.h"
#include "../AppLoader/MSocialUser.h"
#include "MetaHubModels.h"
USING_NS_STD

NS_CL_BEGIN

long long MUtil::getNumberField(stringstream &curRecord)
{
    string recordValue;
    
    getline(curRecord, recordValue, M_KEY_DATA_FIELD_SEPERATOR);
    return atoll(recordValue.data());
}

double MUtil::getFloatField(stringstream &curRecord)
{
    string recordValue;
    getline(curRecord, recordValue, M_KEY_DATA_FIELD_SEPERATOR);
    return atof(recordValue.data());
}

LanguageType MUtil::getCurrentLanguageType(){
    return AppLoader::getInstance()->getLanguageType();
}
std::string MUtil::getCurrentLanguageAlias(){
    return AppLoader::getInstance()->getLanguageAlias();
}

string MUtil::appendCurrentLanguageAliasAsSuffix(std::string aStr){
    if (MUtil::getCurrentLanguageAlias().empty()) {
        return aStr;
    }
    return aStr+"_"+MUtil::getCurrentLanguageAlias();
}

string MUtil::getSerivceURL(string aServiceName){
    return COLIB_APP_SRV_URL+aServiceName;
}

string MUtil::getAppId(){
    return COLIB_APP_ID;
}

string MUtil::getAppSubId(){
    return COLIB_APP_SUB_ID;
}

MSocialUser* MUtil::getSocialUser(){
    return AppLoader::getInstance()->getSocialUser();
}

MGameStateController* MUtil::getGameStateController(){
    return AppLoader::getInstance()->getGameStateController();
}

MConfigResponse* MUtil::getConfigResponse(){
    return AppLoader::getInstance()->getConfigResponse();
}

MUserStateResponse* MUtil::getUserStateResponse(){
    return AppLoader::getInstance()->getUserStateResponse();
}

Dictionary* MUtil::getRequiredParamsDict(){
    
    Dictionary *dictToReturn = new Dictionary();

    MCR_DICT_ADD(dictToReturn, Integer::create(Util::getDevicePlatform()), M_KEY_PLATFORM);
    MCR_DICT_ADD(dictToReturn, String::create(Util::getOSVersion()), M_KEY_OS_VERSION);
    MCR_DICT_ADD(dictToReturn, String::create(Util::getDeviceModel()), M_KEY_DEVICE_MODEL);
    
    MCR_DICT_ADD(dictToReturn, Double::create(Util::getBundledVersion()), M_KEY_CLIENT_VERSION);
    
    MCR_DICT_ADD(dictToReturn, String::create(MUtil::getAppId()), M_KEY_APP_ID);
    MCR_DICT_ADD(dictToReturn, String::create(MUtil::getAppSubId()), M_KEY_APP_SUB_ID);
    
    MSocialUser *socialUser = MUtil::getSocialUser();
    if(socialUser){
         // if user exists fill below
         MCR_DICT_ADD(dictToReturn, String::create(socialUser->getId()), M_KEY_SOCIAL_USER_ID);
         MCR_DICT_ADD(dictToReturn, Integer::create(socialUser->getRole()), M_KEY_SOCIAL_USER_ROLE);
         MCR_DICT_ADD(dictToReturn, String::create(socialUser->getSocialInstanceId()), M_KEY_SOCIAL_INSTANCE_ID);
         MCR_DICT_ADD(dictToReturn, String::create(socialUser->getTag()), M_KEY_SOCIAL_USER_TAG);
         MCR_DICT_ADD(dictToReturn, String::create(socialUser->getLocalClientToken()), M_KEY_CLIENT_TOKEN);
    }
    
    dictToReturn->autorelease();
    return dictToReturn;
}

time_t MUtil::getCurrentTime(){
    return AppLoader::getInstance()->getServerTime();
}


static const char *sMetaFilesPath_ = NULL;
static const char *sAssetsPath_   = NULL;

string MUtil::getMetaFilesWriteablePath(){
    if(!sMetaFilesPath_){
        sMetaFilesPath_ = (new string(CCFileUtils::getInstance()->getWritablePath()+"mfs/"))->c_str();
        Util::createDirectoryStructure(sMetaFilesPath_);
    }
    return sMetaFilesPath_;
}

string MUtil::getAssetsWriteablePath(){
    if(!sAssetsPath_){
        sAssetsPath_ = (new string(CCFileUtils::getInstance()->getWritablePath()+"mass/"))->c_str();
        Util::createDirectoryStructure(sAssetsPath_);
    }
    return sAssetsPath_;
}

string MUtil::getMetaFilePath(string aFileId){
    return MUtil::getMetaFilesWriteablePath()+ aFileId + "/textures.pvr";
}


NS_CL_END
